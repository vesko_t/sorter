import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { SortingGroup } from "./model/sortingGroup";
import { AppService } from "./app.service";
import { SortingData } from "./model/sortingData";
import { MessageService } from "primeng";
import { Comment } from './model/comment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MessageService]

})
export class AppComponent implements OnInit {

  public invoiceProperties = [
    {
      label: "Invoice Number",
      value: "NUMBER"
    },
    {
      label: "Issue Date",
      value: "ISSUE_DATE"
    },
    {
      label: "Pay Date",
      value: "PAY_DATE"
    },
    {
      label: "Client Name",
      value: "CLIENT_NAME"
    },
    {
      label: "Issuer",
      value: "ISSUER"
    },
    {
      label: "Amount Left",
      value: "AMOUNT_LEFT"
    },
    {
      label: "Other",
      value: "other"
    }
  ];
  /*ID, COMMENT, ISSUE_DATE, PAY_DATE, CLIENT_NAME, ISSUER, AMOUNT_LEFT, NUMBER*/

  public dataTypes = [
    {
      label: "Number",
      value: "number"
    },
    {
      label: "Text",
      value: "text"
    },
    {
      label: "Date",
      value: "date"
    }
  ];

  public sortingGroups: Array<SortingGroup> = [];

  public comments: Array<Comment> = [];

  constructor(public appService: AppService, public messageService: MessageService) {
  }

  ngOnInit(): void {
    this.appService.getGroups().subscribe(res => {
      this.sortingGroups = res;
      this.sortingGroups.forEach(sortingGroup => {
        sortingGroup.sortingDataList.forEach(sortingData => {
          sortingData.selectedProperty = this.getPropertyObject(sortingData);
          sortingData.selectedDataType = this.getDataObject(sortingData);
        })
      });
    });
    this.appService.getComments().subscribe(res => {
      this.comments = res.map(comment => {
        comment.properties = this.invoiceProperties.filter(element => element.value === comment.properties)[0];
        return comment;
      });
      console.log(res);
    });
  }

  private getPropertyObject(sortingData): any {
    for (let property of this.invoiceProperties) {
      if (sortingData.invoiceProperties === property.value) {
        return property;
      }
    }
    return this.invoiceProperties[this.invoiceProperties.length - 1];
  }

  private getDataObject(sortingData): any {
    if (sortingData.number === true) {
      return this.dataTypes[0];
    } else if (sortingData.string === true) {
      return this.dataTypes[1];
    } else if (sortingData.date === true) {
      return this.dataTypes[2];
    }
  }

  public addGroup(): void {
    let sortingGroup = new SortingGroup();
    sortingGroup.groupName = 'New Group';
    this.sortingGroups.push(sortingGroup);
  }

  public saveGroup(sortingGroup: SortingGroup): void {
    let shouldSave: boolean = true;
    if (typeof sortingGroup.groupName == 'undefined' || sortingGroup.groupName == '') {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Group name is empty!' });
      shouldSave = false;
    } else if (typeof sortingGroup.sortingDataList == 'undefined' || sortingGroup.sortingDataList.length == 0) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Add Sorting Data' });
      shouldSave = false;
    }
    for (let sortingData of sortingGroup.sortingDataList) {
      if (typeof sortingData.selectedProperty == 'undefined' || sortingData.selectedProperty == null) {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Select Input data' });
        shouldSave = false;
      } else if (sortingData.number && (sortingData.numberValue == null || typeof sortingData.numberValue == 'undefined')) {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Compare value is empty' });
        shouldSave = false;
      } else if (sortingData.string && (sortingData.stringValue == null || typeof sortingData.numberValue == 'undefined' || sortingData.stringValue == '')) {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Compare value is empty' });
        shouldSave = false;
      } else if (sortingData.date && (sortingData.dateValue == null || typeof sortingData.dateValue == 'undefined')) {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Compare value is empty' });
        shouldSave = false;
      } else if ((sortingData.selectedDataType == null || typeof sortingData.selectedDataType == 'undefined') && sortingData.invoiceProperties === 'other') {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Select Data type' });
        shouldSave = false;
      }
    }
    if (shouldSave) {
      this.appService.saveGroup(sortingGroup).subscribe(res => {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Group Saved' });
      });
    }
  }

  public deleteGroup(sortingGroup: SortingGroup): void {
    let index = this.sortingGroups.indexOf(sortingGroup);
    this.sortingGroups.splice(index, 1);
    if (typeof sortingGroup.id != 'undefined') {
      this.appService.deleteGroup(sortingGroup).subscribe(res => {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Group Deleted' });

      });
    }

  }

  public deleteData(sortingGroup: SortingGroup, sortingData: SortingData) {
    let index = sortingGroup.sortingDataList.indexOf(sortingData);
    sortingGroup.sortingDataList.splice(index, 1);
    if (typeof sortingData.id != 'undefined') {
      this.appService.deleteData(sortingData).subscribe(res => {
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Data Deleted' });
      });
    }
  }

  public addData(sortingGroup: SortingGroup): void {
    sortingGroup.sortingDataList.push(new SortingData());
  }

  public onPropertyChange(event, sortingData: SortingData): void {
    let value = event.value.value;

    if (value == 'COMMENT' || value == 'CLIENT_NAME' || value == 'ISSUER' || value == 'NUMBER') {
      sortingData.string = true;
      sortingData.date = false;
      sortingData.number = false;
    } else if (value == 'ISSUE_DATE' || value == 'PAY_DATE') {
      sortingData.string = false;
      sortingData.date = true;
      sortingData.number = false;
    } else if (value == 'AMOUNT_LEFT') {
      sortingData.string = false;
      sortingData.date = false;
      sortingData.number = true;
    } else {
      sortingData.collNumber = null;
    }

    if (value != 'other') {
      sortingData.invoiceProperties = value;
      sortingData.collNumber = -1;
    }
  }

  public onDataTypeChange(event, sortingData: SortingData): void {
    let value = event.value.value;
    if (value === 'text') {
      sortingData.string = true;
      sortingData.date = false;
      sortingData.number = false;
    } else if (value === 'number') {
      sortingData.string = false;
      sortingData.date = false;
      sortingData.number = true;
    } else if (value == 'date') {
      sortingData.string = false;
      sortingData.date = true;
      sortingData.number = false;
    }
  }

  addComment() {
    this.comments.push(new Comment());
  }

  saveComment(comment: Comment, index: number) {
    comment.properties = comment.properties['value'];
    console.log(comment);
    this.appService.saveComment(comment).subscribe(res => {
      comment = res;
      comment.properties = this.invoiceProperties.filter(element => element.value === comment.properties)[0];
      this.comments[index] = comment;
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Comment Saved' });
    });
  }

  deleteComment(comment: Comment) {
    if (typeof comment.id !== 'undefined') {
      this.appService.deleteComment(comment.id).subscribe(res => {
        this.comments.splice(this.comments.indexOf(comment), 1);
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Comment Deleted' });
      });
    } else {
      this.comments.splice(this.comments.indexOf(comment), 1);
    }
  }

}

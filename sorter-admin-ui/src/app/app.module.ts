import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppService } from "./app.service";
import { HttpClientModule } from "@angular/common/http";

import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from "primeng/accordion";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ToastModule } from 'primeng/toast';
import { RadioButtonModule } from 'primeng/radiobutton';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ToolbarModule,
    ButtonModule,
    AccordionModule,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    InputTextModule,
    PanelModule,
    DropdownModule,
    CalendarModule,
    CheckboxModule,
    ToastModule,
    RadioButtonModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

/**
 * Created by vesko on 05.03.20.
 */
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SortingGroup } from "./model/sortingGroup";
import { SortingData } from "./model/sortingData";
import { Comment } from "./model/comment";

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private http: HttpClient) {

  }

  public getGroups(): Observable<Array<SortingGroup>> {
    let url = '/api/get-groups';
    return this.http.get<Array<SortingGroup>>(url);
  }

  public saveGroup(sortingGroup: SortingGroup): Observable<any> {
    let url = '/api/save-group';
    return this.http.post(url, sortingGroup);
  }

  public deleteGroup(sortingGroup: SortingGroup): Observable<any> {
    let url = '/api/delete-group';
    return this.http.post(url, sortingGroup);
  }

  public deleteData(sortingData: SortingData): Observable<any> {
    let url = '/api/delete-data';
    return this.http.post(url, sortingData);
  }

  public getComments(): Observable<Array<Comment>> {
    let url = '/api/comments';

    return this.http.get<Array<Comment>>(url);
  }

  public saveComment(comment: Comment): Observable<Comment> {
    let url = '/api/comments';

    return this.http.post<Comment>(url, comment);
  }

  public deleteComment(id: number): Observable<any> {
    let url = '/api/comments/' + id;

    return this.http.delete(url);
  }
}

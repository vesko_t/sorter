export class Comment {
  id: number;
  property: string;
  value: string;
  properties: string | { value: string, label: string };
  income: boolean;
}

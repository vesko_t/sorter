/**
 * Created by vesko on 05.03.20.
 */
export class SortingData {

  id: number;

  dateValue: Date = null;

  numberValue: number = null;

  stringValue: string = null;

  invoiceProperties: string;

  collNumber: number = -1;

  required: boolean = false;

  selectedProperty: any;

  selectedDataType: any;

  date: boolean;

  number: boolean;

  string: boolean;

}

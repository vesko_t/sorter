import { SortingData } from "./sortingData";
/**
 * Created by vesko on 05.03.20.
 */
export class SortingGroup {

  id: number;

  groupName: string;

  sortingDataList: Array<SortingData> = [];
}

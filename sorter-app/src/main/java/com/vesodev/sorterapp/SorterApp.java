package com.vesodev.sorterapp;

import com.vesodev.sorterapp.gui.Frame;
import com.vesodev.sorterapp.services.ServerConnector;

import javax.swing.*;

/**
 * Created by Vesko on 3.3.2018 г..
 */
public class SorterApp {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.jgoodies.looks.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    try {
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        return;
                    }
                }
            }
        }

        ServerConnector.init();
        System.out.println("Server initialized");

        Frame frame = new Frame("Sorter v20.03.3");
    }

}

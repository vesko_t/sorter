package com.vesodev.sorterapp.gui;

import javax.swing.*;

/**
 * Created by Vesko on 4.3.2018 г..
 */
public class BrowseButton extends JButton {
    public BrowseButton() {
        super("Избери файл");
        setSize(100, 25);
        setVisible(true);
        setLocation(Frame.getFrameSize().width / 2 + 50, (Frame.getFrameSize().height / 3)/2);
    }
}

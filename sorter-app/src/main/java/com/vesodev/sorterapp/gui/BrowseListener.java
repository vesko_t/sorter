package com.vesodev.sorterapp.gui;

import com.vesodev.sorterapp.utils.SorterUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by Vesko on 4.3.2018 г..
 */
class BrowseListener implements ActionListener {

    private BrowsePanel browsePanel;
    private String incomeDirectory;
    private String costDirectory;
    private JCheckBox dateBox;
    private JFormattedTextField daysField;
    private JFrame frame;

    BrowseListener(BrowsePanel browsePanel, String incomeDirectory, String costDirectory, JCheckBox dateBox, JFormattedTextField daysField, JFrame frame) {
        this.browsePanel = browsePanel;
        this.incomeDirectory = incomeDirectory;
        this.costDirectory = costDirectory;
        this.dateBox = dateBox;
        this.daysField = daysField;
        this.frame = frame;
    }

    public void actionPerformed(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel files(.xls, .xlsx)", "xlsx", "xls");
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setFileFilter(filter);
        if (e.getActionCommand().equals("INCOME")) {
            if (incomeDirectory != null) {
                fileChooser.setCurrentDirectory(new File(incomeDirectory));
            }
        } else {
            if (Integer.parseInt(daysField.getText()) <= 1) {
                SorterUtils.sendError("ГРЕШКА!", "Периодът трябва да е по-голям от 1!", frame);
                return;
            }
            if (costDirectory != null) {
                fileChooser.setCurrentDirectory(new File(costDirectory));
            }
        }
        fileChooser.setVisible(true);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.showOpenDialog(browsePanel);
        browsePanel.getTextField().setText(fileChooser.getSelectedFile().toString());

        if (e.getActionCommand().equals("INCOME")) {
            SorterUtils.saveIncomeData(fileChooser.getSelectedFile(), dateBox.isSelected());
            browsePanel.setIncomeFile(fileChooser.getSelectedFile());
        } else {
            SorterUtils.saveCostDirectory(fileChooser.getSelectedFile(), Integer.parseInt(daysField.getText()));
            browsePanel.setCostFile(fileChooser.getSelectedFile());
        }
    }
}

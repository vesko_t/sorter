package com.vesodev.sorterapp.gui;

import com.vesodev.sorterapp.utils.SorterUtils;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Vesko on 4.3.2018 г..
 */
public class BrowsePanel extends JPanel implements ActionListener {

    private JTextField textField;
    private Frame frame;
    private boolean isIncome;
    private HelpDialog helpDialog;

    public BrowsePanel(Frame frame, boolean isIncome) {
        this.frame = frame;
        this.isIncome = isIncome;
        helpDialog = new HelpDialog(frame, SorterUtils.loadDoc(isIncome));

        BrowseButton browseButton = new BrowseButton();
        browseButton.setActionCommand(isIncome ? "INCOME" : "COST");
        browseButton.addActionListener(new BrowseListener(this, frame.getIncomeDirectory(), frame.getCostDirectory(), frame.getDateBox(),frame.getDaysField(),frame));
        //browseButton.setSize(new Dimension(100,25));
        textField = new JTextField();
        Border solid = BorderFactory.createLineBorder(Color.black);
        Border padding = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        textField.setSize(new Dimension(200, 25));
        textField.setBorder(BorderFactory.createCompoundBorder(solid, padding));
        textField.setLocation(10, browseButton.getLocation().y);
        setSize(Frame.getFrameSize().width, Frame.getFrameSize().height / 3);

        JLabel title = new JLabel(isIncome ? "Вземания" : "Давания");
        title.setSize(100, 25);
        title.setLocation((Frame.getFrameSize().width / 2) - (title.getSize().width / 2), 10);
        title.setVisible(true);

        JButton help = new JButton(new ImageIcon(BrowsePanel.class.getResource("/images/question.png")));
        help.setSize(25, 25);
        help.setLocation(364, 0);
        help.addActionListener(this);

        setLayout(null);
        add(title);
        add(textField);
        add(browseButton);
        add(help);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        helpDialog.setVisible(true);
    }

    public void setCostFile(File file) {
        frame.setCostFile(file);
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setIncomeFile(File file) {
        frame.setIncomeFile(file);
    }
}

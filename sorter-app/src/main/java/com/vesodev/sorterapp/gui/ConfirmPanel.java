package com.vesodev.sorterapp.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Vesko on 4.3.2018 г..
 */
public class ConfirmPanel extends JPanel {

    public ConfirmPanel(Frame frame, boolean isIncome) {
        setLayout(new GridLayout(3, 3));
        JButton button = new JButton("Генериране");
        setLocation((Frame.getFrameSize().width / 2) - 50, 96);
        button.setSize(100, 25);
        button.setVisible(true);
        button.setActionCommand(isIncome?"INCOME":"COST");
        button.addActionListener(new GenerateListener(frame));
        add(new JPanel());
        add(new JPanel());
        add(new JPanel());
        add(new JPanel());
        add(button);
        add(new JPanel());
        add(new JPanel());
        add(new JPanel());
        add(new JPanel());
    }
}

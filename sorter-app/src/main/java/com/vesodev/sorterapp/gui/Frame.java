package com.vesodev.sorterapp.gui;

import com.vesodev.sorterapp.utils.CostData;
import com.vesodev.sorterapp.utils.IncomeData;
import com.vesodev.sorterapp.utils.SorterUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

/**
 * Created by Vesko on 4.3.2018 г..
 */
public class Frame extends JFrame implements ActionListener {

    private static final Dimension frameSize = new Dimension(400, 300);
    private File incomeFile = null;
    private BrowsePanel incomeBrowsePanel;
    private JCheckBox dateBox;
    private String incomeDirectory;

    private File costFile = null;
    private BrowsePanel costBrowsePanel;
    private String costDirectory;
    private JFormattedTextField daysField;
    private SettingsDialog settingsDialog;

    public Frame(String title) throws HeadlessException {
        super(title);
        setResizable(false);

        settingsDialog = new SettingsDialog(this);

        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("Файл");
        JMenuItem settingsItem = new JMenuItem("Настройки");
        settingsItem.setActionCommand("settings");
        settingsItem.addActionListener(this);
        JMenuItem exitItem = new JMenuItem("Изход");
        exitItem.setActionCommand("exit");
        exitItem.addActionListener(this);

        fileMenu.add(settingsItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        JTabbedPane tabbedPane = new JTabbedPane();

        dateBox = new JCheckBox("Промяна Дата Падеж", true);
        dateBox.setSize(200, 25);
        dateBox.setLocation(10, (int) (Frame.getFrameSize().height / 3.5f));

        NumberFormat customFormat = NumberFormat.getIntegerInstance();
        customFormat.setMinimumIntegerDigits(1);
        customFormat.setMaximumIntegerDigits(3);
        customFormat.setParseIntegerOnly(true);
        daysField = new JFormattedTextField(customFormat);
        daysField.setSize(40, 25);
        daysField.setLocation(65, (int) (Frame.getFrameSize().height / 3.5f));
        JLabel periodLabel = new JLabel("Период");
        periodLabel.setSize(50, 25);
        periodLabel.setLocation(10, (int) (Frame.getFrameSize().height / 3.5f));

        IncomeData incomeData = SorterUtils.loadIncomeData();
        if (incomeData != null) {
            dateBox.setSelected(incomeData.isModifyPayDate());
            incomeDirectory = incomeData.getDirectory();
        }

        CostData costData = SorterUtils.loadCostData();
        if (costData != null) {
            daysField.setText(String.valueOf(costData.getDaysPeriod()));
            costDirectory = costData.getDirectory();
        } else {
            daysField.setText("7");
        }

        JPanel incomePanel = new JPanel(new GridLayout(2, 1));
        incomeBrowsePanel = new BrowsePanel(this, true);
        incomeBrowsePanel.add(dateBox);
        incomePanel.add(incomeBrowsePanel);
        incomePanel.add(new ConfirmPanel(this, true));

        JPanel costPanel = new JPanel(new GridLayout(2, 1));
        costBrowsePanel = new BrowsePanel(this, false);
        costBrowsePanel.add(daysField);
        costBrowsePanel.add(periodLabel);
        costPanel.add(costBrowsePanel);
        costPanel.add(new ConfirmPanel(this, false));

        setSize(frameSize);
        try {
            setIconImage(ImageIO.read(Frame.class.getResource("/images/icon64.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        GridLayout layout = new GridLayout(1, 1);
        setLayout(layout);
        tabbedPane.addTab("Вземания", new ImageIcon(Frame.class.getResource("/images/arrow-up.png")), incomePanel);
        tabbedPane.addTab("Давания", new ImageIcon(Frame.class.getResource("/images/arrow-down.png")), costPanel);
        add(tabbedPane);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals("settings")) {
            settingsDialog.setVisible(true);
        } else if (actionEvent.getActionCommand().equals("exit")) {
            System.exit(0);
        }
    }

    File getIncomeFile() {
        return incomeFile;
    }

    static Dimension getFrameSize() {
        return frameSize;
    }

    void setIncomeFile(File incomeFile) {
        this.incomeFile = incomeFile;
    }

    BrowsePanel getIncomeBrowsePanel() {
        return incomeBrowsePanel;
    }

    void setCostFile(File costFile) {
        this.costFile = costFile;
    }

    File getCostFile() {
        return costFile;
    }

    JCheckBox getDateBox() {
        return dateBox;
    }

    String getIncomeDirectory() {
        return incomeDirectory;
    }

    String getCostDirectory() {
        return costDirectory;
    }

    BrowsePanel getCostBrowsePanel() {
        return costBrowsePanel;
    }

    JFormattedTextField getDaysField() {
        return daysField;
    }
}

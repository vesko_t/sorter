package com.vesodev.sorterapp.gui;

import com.vesodev.sorterapp.services.CostSorter;
import com.vesodev.sorterapp.services.IncomeSorter;
import com.vesodev.sorterapp.utils.SorterUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Vesko on 4.3.2018 г..
 */
public class GenerateListener implements ActionListener {

    private Frame frame;
    private File file;

    public GenerateListener(Frame frame) {
        this.frame = frame;
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("INCOME")) {
            file = frame.getIncomeFile();
            if (file == null) {
                file = new File(frame.getIncomeBrowsePanel().getTextField().getText());
            }
            Thread thread = new Thread(() -> {
                try {
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    IncomeSorter.sort(file, frame.getDateBox().isSelected());
                } catch (Exception ex) {
                    SorterUtils.sendError("Грешка", "Възникна грешка!", frame);
                    ex.printStackTrace();
                } finally {
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                }
            });
            thread.start();
        } else {
            file = frame.getCostFile();
            if (file == null) {
                file = new File(frame.getCostBrowsePanel().getTextField().getText());
            }
            Thread thread = new Thread(() -> {
                try {
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    CostSorter.sort(file, Integer.parseInt(frame.getDaysField().getText()));
                } catch (Exception ex) {
                    SorterUtils.sendError("Грешка", "Възникна грешка!", frame);
                    ex.printStackTrace();
                } finally {
                    frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                }
            });
            thread.start();
        }
    }
}

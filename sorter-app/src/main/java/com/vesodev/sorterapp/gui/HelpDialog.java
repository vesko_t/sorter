package com.vesodev.sorterapp.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.Frame;

public class HelpDialog extends JDialog {

    public HelpDialog(Frame owner, String doc) {
        super(owner, true);
        setResizable(false);
        setSize(400, 600);
        setLocationRelativeTo(null);
        JTextArea textArea = new JTextArea(doc);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);

        JScrollPane scroll = new JScrollPane(textArea);
        add(scroll);
    }

}

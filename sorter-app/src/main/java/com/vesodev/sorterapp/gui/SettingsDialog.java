package com.vesodev.sorterapp.gui;

import com.vesodev.sorterapp.utils.ServerData;
import com.vesodev.sorterapp.utils.SorterUtils;

import javax.swing.*;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by vesko on 09.03.20.
 */
public class SettingsDialog extends JDialog implements ActionListener {

    private JLabel ipLabel;
    private JTextField ipField;
    private JButton applyButton;
    private JButton cancelButton;
    private JLabel portLabel;
    private JTextField portField;

    public SettingsDialog(Frame frame) {
        super(frame);
        setVisible(false);
        setTitle("Настройки");
        setResizable(false);
        setSize(220, 200);
        setLocationRelativeTo(null);
        setLayout(null);

        ServerData serverData = SorterUtils.getServerIp();

        ipLabel = new JLabel("Адрес а сървъра(IP)");
        ipLabel.setSize(300, 25);
        ipLabel.setLocation(10, 10);
        add(ipLabel);

        ipField = new JTextField();
        ipField.setSize(200, 25);
        ipField.setLocation(10, 40);
        ipField.setText(serverData.getIp());
        add(ipField);

        portLabel = new JLabel("Порт");
        portLabel.setSize(300, 25);
        portLabel.setLocation(10, 75);
        add(portLabel);

        portField = new JTextField();
        portField.setSize(200, 25);
        portField.setLocation(10, 100);
        portField.setText(serverData.getPort());
        add(portField);

        applyButton = new JButton("Запази");
        applyButton.setSize(95, 25);
        applyButton.setLocation(10, 135);
        applyButton.setActionCommand("apply");
        applyButton.addActionListener(this);
        add(applyButton);

        cancelButton = new JButton("Отказ");
        cancelButton.setSize(95, 25);
        cancelButton.setLocation(115, 135);
        cancelButton.setActionCommand("cancel");
        cancelButton.addActionListener(this);
        add(cancelButton);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals("apply")) {
            String ip = ipField.getText();
            String port = portField.getText();
            if (ip == null || port == null || port.equals("") || ip.equals("")) {
                SorterUtils.sendError("Грешка", "Моля не оставяйте празни полета", this);
            } else {
                SorterUtils.saveServerInfo(ip, port);
                JOptionPane.showMessageDialog(this, "Настройките са запазени!", "Успех", JOptionPane.INFORMATION_MESSAGE);
                setVisible(false);
            }
        } else if (actionEvent.getActionCommand().equals("cancel")) {
            setVisible(false);
        }
    }
}

package com.vesodev.sorterapp.model;

import java.util.Date;

/**
 * Created by Aossia on 2/21/2020.
 */
public class ColumnValue {

    private Double doubleValue;

    private Date dateValue;

    private String stringValue;

    public ColumnValue(Double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public ColumnValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public ColumnValue(String stringValue) {
        this.stringValue = stringValue;
    }


    public Double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(Double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }
}

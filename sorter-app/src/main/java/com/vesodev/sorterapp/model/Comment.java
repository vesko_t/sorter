package com.vesodev.sorterapp.model;

public class Comment {

    private Integer id;

    private String property;

    private String value;

    private InvoiceProperties properties;

    private Boolean income;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public InvoiceProperties getProperties() {
        return properties;
    }

    public void setProperties(InvoiceProperties properties) {
        this.properties = properties;
    }

    public Boolean getIncome() {
        return income;
    }

    public void setIncome(Boolean income) {
        this.income = income;
    }
}

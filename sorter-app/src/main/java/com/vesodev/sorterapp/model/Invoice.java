package com.vesodev.sorterapp.model;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vesko on 18.05.18.
 */
public class Invoice {

    public static final String USD = "USD";
    public static final String BGN = "BGN";
    public static final String BGNOther = "ЛВ.";
    public static final String EUR = "EUR";
    public static final String GBP = "GBP";

    private int id;
    private String comment;
    private Date issueDate;
    private Date payDate;
    private String clientName;
    private String issuer;
    private String currency;
    private double amountLeft;
    private String number;
    private boolean hasProperties;

    private Map<Integer, ColumnValue> additionalValues;

    public Invoice() {
    }

    public Invoice(Invoice invoice) {
        id = invoice.id;
        comment = invoice.comment;
        issueDate = invoice.issueDate;
        payDate = invoice.payDate;
        clientName = invoice.clientName;
        issuer = invoice.issuer;
        currency = invoice.currency;
        amountLeft = invoice.amountLeft;
        number = invoice.number;
        hasProperties = invoice.hasProperties;
        if (invoice.additionalValues != null) {
            additionalValues = new HashMap<>(invoice.additionalValues);
        }
    }

    public Invoice(int id, String comment, Date issueDate, String clientName, String issuerl, String currency, double amountLeft) {
        this.id = id;
        this.comment = comment;
        this.issueDate = issueDate;
        this.clientName = clientName;
        this.issuer = issuerl;
        this.currency = currency;
        this.amountLeft = amountLeft;
    }

    public Invoice(int id, Date issueDate, Date payDate, String clientName, String issuer, String currency, double amountLeft) {
        this.id = id;
        this.issueDate = issueDate;
        this.payDate = payDate;
        this.clientName = clientName;
        this.issuer = issuer;
        this.currency = currency;
        this.amountLeft = amountLeft;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.YYYY");
        return clientName + "\t" + simpleDateFormat.format(payDate) + "\t" + amountLeft + "\t" + number + "\t" + simpleDateFormat.format(issueDate) + "\t" + currency;
    }

    @Override
    public boolean equals(Object obj) {
        return clientName.equals(((Invoice) obj).clientName) && number.equals(((Invoice) obj).number) && issueDate.equals(((Invoice) obj).issueDate);
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuerl) {
        this.issuer = issuerl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmountLeft() {
        return amountLeft;
    }

    public void setAmountLeft(double amountLeft) {
        this.amountLeft = amountLeft;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void appendComment(String comment) {
        if (this.comment == null) {
            this.comment = "";
        }else {
            this.comment += ", ";
        }
        this.comment += comment;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = DateUtils.round(issueDate, Calendar.DAY_OF_MONTH);
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = DateUtils.round(payDate, Calendar.DAY_OF_MONTH);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Map<Integer, ColumnValue> getAdditionalValues() {
        if (additionalValues == null) {
            additionalValues = new HashedMap<>();
        }
        return additionalValues;
    }

    public void setAdditionalValues(Map<Integer, ColumnValue> additionalValues) {
        this.additionalValues = additionalValues;
    }

    public boolean hasProperties() {
        hasProperties = additionalValues != null && additionalValues.size() > 0;
        return hasProperties;
    }
}

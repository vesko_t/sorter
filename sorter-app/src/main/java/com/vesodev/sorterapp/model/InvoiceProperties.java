package com.vesodev.sorterapp.model;

/**
 * Created by Aossia on 2/21/2020.
 */
public enum InvoiceProperties {
    ID, COMMENT, ISSUE_DATE, PAY_DATE, CLIENT_NAME, ISSUER, AMOUNT_LEFT, NUMBER
}

package com.vesodev.sorterapp.model;

import java.util.Date;

/**
 * Created by Aossia on 2/21/2020.
 */
public class SortingData {
    private Date dateValue;

    private Double numberValue;

    private String stringValue;

    private InvoiceProperties invoiceProperties;

    private Integer collNumber;

    private boolean isDate;

    private boolean isNumber;

    private boolean isString;

    private boolean required;

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public Double getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(Double numberValue) {
        this.numberValue = numberValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public InvoiceProperties getInvoiceProperties() {
        return invoiceProperties;
    }

    public void setInvoiceProperties(InvoiceProperties invoiceProperties) {
        this.invoiceProperties = invoiceProperties;
    }

    public Integer getCollNumber() {
        return collNumber;
    }

    public void setCollNumber(Integer collNumber) {
        this.collNumber = collNumber;
    }

    public boolean isDate() {
        return isDate;
    }

    public void setDate(boolean date) {
        isDate = date;
    }

    public boolean isNumber() {
        return isNumber;
    }

    public void setNumber(boolean number) {
        isNumber = number;
    }

    public boolean isString() {
        return isString;
    }

    public void setString(boolean string) {
        isString = string;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}

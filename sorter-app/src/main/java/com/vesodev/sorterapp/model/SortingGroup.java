package com.vesodev.sorterapp.model;

import java.util.List;

/**
 * Created by Aossia on 2/21/2020.
 */
public class SortingGroup {

    private String groupName;

    private List<SortingData> sortingDataList;


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<SortingData> getSortingDataList() {
        return sortingDataList;
    }

    public void setSortingDataList(List<SortingData> sortingDataList) {
        this.sortingDataList = sortingDataList;
    }

}

package com.vesodev.sorterapp.services;

import com.vesodev.sorterapp.model.*;
import com.vesodev.sorterapp.utils.SorterUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Aossia on 2/21/2020.
 */
public final class CostExtractor {

    private CostExtractor() {

    }

    public static List<Invoice> extractData(Workbook workbook, List<SortingGroup> sortingGroups, List<Comment> comments) throws Exception {
        List<Invoice> invoices = new ArrayList<>();
        List<Comment> numberComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.NUMBER.equals(comment.getProperties()) && !comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();
        List<Comment> issuerComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.ISSUER.equals(comment.getProperties()) && !comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();
        List<Comment> clientComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.CLIENT_NAME.equals(comment.getProperties()) && !comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();
        List<Comment> amountComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.AMOUNT_LEFT.equals(comment.getProperties()) && !comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();

        int i = 0;
        for (Row row : workbook.getSheetAt(workbook.getActiveSheetIndex())) {
            if (i > 0) {
                Invoice invoice = new Invoice();
                if (row.getCell(1) != null && row.getCell(1).getStringCellValue() != null && !row.getCell(1).getStringCellValue().equals("")) {
                    invoice.setClientName(row.getCell(1).getStringCellValue());
                    for (Comment comment : clientComments) {
                        if (SorterUtils.areEqual(invoice.getClientName(), comment.getProperty())) {
                            invoice.appendComment(comment.getValue());
                        }
                    }
                }

                if (row.getCell(8) != null && row.getCell(8).getDateCellValue() != null) {
                    invoice.setPayDate(row.getCell(8).getDateCellValue());
                }

                if (row.getCell(6) != null && row.getCell(6).getNumericCellValue() != 0) {
                    invoice.setAmountLeft(row.getCell(6).getNumericCellValue());
                    for (Comment comment : amountComments) {
                        if (String.valueOf(invoice.getAmountLeft()).equals(comment.getProperty())) {
                            invoice.appendComment(comment.getValue());
                        }
                    }
                }

                if (row.getCell(2) != null && row.getCell(2).getStringCellValue() != null && !row.getCell(2).getStringCellValue().equals("")) {
                    invoice.setNumber(row.getCell(2).getStringCellValue());
                    for (Comment comment : numberComments) {
                        if (invoice.getNumber().equals(comment.getProperty())) {
                            invoice.appendComment(comment.getValue());
                        }
                    }
                }

                if (row.getCell(3) != null && row.getCell(3).getDateCellValue() != null) {
                    invoice.setIssueDate(row.getCell(3).getDateCellValue());
                }

                if (row.getCell(7) != null && row.getCell(7).getStringCellValue() != null && !row.getCell(7).getStringCellValue().equals("")) {
                    invoice.setCurrency(row.getCell(7).getStringCellValue());
                }

                if (row.getCell(13) != null && row.getCell(13).getStringCellValue() != null && !row.getCell(13).getStringCellValue().equals("")) {
                    invoice.setIssuer(row.getCell(13).getStringCellValue());
                    for (Comment comment : issuerComments) {
                        if (invoice.getIssuer().equals(comment.getProperty())) {
                            invoice.appendComment(comment.getValue());
                        }
                    }
                }

                if (sortingGroups != null) {
                    SorterUtils.extractCustomData(sortingGroups, row, invoice);
                }
                invoices.add(invoice);
            }
            i++;
        }

        return invoices;
    }

    public static List<Invoice> getGroupInvoices(SortingGroup sortingGroup, List<Invoice> invoices) {
        List<Invoice> groupInvoices = new ArrayList<>();
        for (Invoice invoice : invoices) {
            for (SortingData sortingData : sortingGroup.getSortingDataList()) {
                //TODO finish condition with Sasho
            }
        }
        return groupInvoices;
    }

}

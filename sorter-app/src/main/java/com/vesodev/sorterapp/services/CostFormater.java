package com.vesodev.sorterapp.services;

import com.vesodev.sorterapp.model.Invoice;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Aossia on 2/21/2020.
 */
public class CostFormater {

    private static CellStyle boldArialFrame12;
    private static CellStyle boldArialFrame10;
    private static CellStyle boldArial12;
    private static CellStyle arialFrame10;
    private static CellStyle boldArial12Centered;
    private static CellStyle boldArial10Centered;
    private static CellStyle italicArial10Centered;
    private static CellStyle leftArial10Thick;
    private static CellStyle arial10Thick;
    private static CellStyle righArial10Thick;

    private static double oUsdSum = 0;
    private static double oEurSum = 0;
    private static double oBgnSum = 0;

    private static double usdSum = 0;
    private static double eurSum = 0;
    private static double bgnSum = 0;

    private CostFormater() {

    }

    public static void formatAndCreateSheet(Map<String, List<List<Invoice>>> invoicesFiltered, Workbook workbook, String sheetName, String overdueSheetName, int days) {
        intiStyles(workbook);
        if (boldArial10Centered != null) {
            oUsdSum = 0;
            oEurSum = 0;
            oBgnSum = 0;

            usdSum = 0;
            eurSum = 0;
            bgnSum = 0;
        }

        Map<String, List<List<Invoice>>> overdue = getOverdue(invoicesFiltered, days);

        generateSheet(invoicesFiltered, workbook, sheetName);
        generateSheet(overdue, workbook, overdueSheetName);

    }

    private static Map<String, List<List<Invoice>>> getOverdue(Map<String, List<List<Invoice>>> invoices, int days) {
        Map<String, List<List<Invoice>>> overdue = new HashMap<>();
        invoices.forEach((key, currency) -> {
            List<List<Invoice>> overdueCurrency = new ArrayList<>();
            for (int i = 0; i < currency.size(); i++) {
                List<Invoice> overdueCompany = new ArrayList<>();
                for (int j = 0; j < currency.get(i).size(); j++) {
                    if (isOverdue(currency.get(i).get(j), days)) {
                        overdueCompany.add(currency.get(i).get(j));
                        if (key.equals(Invoice.USD))
                            oUsdSum += currency.get(i).get(j).getAmountLeft();
                        if (key.equals(Invoice.EUR))
                            oEurSum += currency.get(i).get(j).getAmountLeft();
                        if (key.equals(Invoice.BGNOther))
                            oBgnSum += currency.get(i).get(j).getAmountLeft();
                    }
                    if (key.equals(Invoice.USD))
                        usdSum += currency.get(i).get(j).getAmountLeft();
                    if (key.equals(Invoice.EUR))
                        eurSum += currency.get(i).get(j).getAmountLeft();
                    if (key.equals(Invoice.BGNOther))
                        bgnSum += currency.get(i).get(j).getAmountLeft();
                }
                if (overdueCompany.size() > 0)
                    overdueCurrency.add(overdueCompany);
            }
            overdue.put(key, overdueCurrency);
        });
        return overdue;
    }

    private static boolean isOverdue(Invoice invoice, int days) {
        return invoice.getPayDate().getTime() <= new Date().getTime() + (8.64e+7 * (days - 1));
    }

    private static void generateSheet(Map<String, List<List<Invoice>>> invoicesFiltered, Workbook workbook, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        setHeader(sheet, sheetName.equals(IncomeSorter.overdueName), workbook);

        addSmallHeader(sheet, 10);
        int totalRows = 11;

        for (int k = 0; k < 3; k++) {
            List<List<Invoice>> invoiceBlocks;
            if (k == 0)
                invoiceBlocks = invoicesFiltered.get(Invoice.USD);
            else if (k == 1)
                invoiceBlocks = invoicesFiltered.get(Invoice.EUR);
            else
                invoiceBlocks = invoicesFiltered.get(Invoice.BGNOther);

            for (List<Invoice> invoices : invoiceBlocks) {
                int num = 0;
                for (int i = 0; i < invoices.size(); i++) {
                    Invoice invoice = invoices.get(i);
                    Row row = sheet.createRow(totalRows);

                    fillRow(row, invoice, invoices.size() == 1);
                    totalRows++;
                    num++;
                }
                if (num > 1) {
                    Row row = sheet.createRow(totalRows);
                    Cell cell = row.createCell(1);
                    cell.setCellValue("Общо:");
                    cell.setCellStyle(boldArialFrame10);
                    Cell total = row.createCell(2);
                    total.setCellType(CellType.FORMULA);
                    total.setCellFormula("SUM(" +
                            sheet.getRow(totalRows - 1).getCell(2).getAddress() + ":" +
                            sheet.getRow(totalRows - num).getCell(2).getAddress() + ")");
                    total.setCellStyle(boldArialFrame10);

                    totalRows++;
                }

                totalRows++;
            }
            if (k != 2) {
                addSmallHeader(sheet, totalRows + 2);
                totalRows += 3;
            }
        }
    }

    private static void fillRow(Row row, Invoice invoice, boolean isTheOnlyOne) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        Cell name = row.createCell(0);
        name.setCellValue(invoice.getClientName());
        name.setCellStyle(arialFrame10);

        Cell payDate = row.createCell(1);
        payDate.setCellValue(dateFormat.format(invoice.getPayDate()));
        payDate.setCellStyle(arialFrame10);

        Cell amount = row.createCell(2);
        amount.setCellValue(invoice.getAmountLeft());
        amount.setCellStyle(isTheOnlyOne ? boldArialFrame10 : arialFrame10);

        Cell number = row.createCell(3);
        number.setCellValue(invoice.getNumber());
        number.setCellStyle(arialFrame10);

        Cell issueDate = row.createCell(4);
        issueDate.setCellValue(dateFormat.format(invoice.getIssueDate()));
        issueDate.setCellStyle(arialFrame10);

        if (invoice.getComment() != null && !"".equals(invoice.getComment())) {
            Cell comment = row.createCell(5);
            comment.setCellValue(invoice.getComment());
        }
    }

    private static void addSmallHeader(Sheet sheet, int rowNum) {
        Row infoRow = sheet.createRow(rowNum);
        Cell name = infoRow.createCell(0);
        name.setCellValue("Подизпълнител");
        name.setCellStyle(leftArial10Thick);

        Cell id = infoRow.createCell(1);
        id.setCellValue("Падеж");
        id.setCellStyle(arial10Thick);

        Cell date = infoRow.createCell(2);
        date.setCellValue("Сума");
        date.setCellStyle(arial10Thick);

        Cell ammount = infoRow.createCell(3);
        ammount.setCellValue("Номер Ф-ра");
        ammount.setCellStyle(arial10Thick);

        Cell currency = infoRow.createCell(4);
        currency.setCellValue("Дата");
        currency.setCellStyle(righArial10Thick);
    }

    private static void setHeader(Sheet sheet, boolean isOverdue, Workbook workbook) {
        Row head = sheet.createRow(0);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
        Cell headCell = head.createCell(0);
        headCell.setCellValue("Справка Задължения");
        headCell.setCellStyle(boldArial12Centered);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 4));

        Row head2 = sheet.createRow(1);
        Cell headCell2 = head2.createCell(0);
        headCell2.setCellValue(isOverdue ? "до " + new SimpleDateFormat("dd.MM.YYYY").format(new Date((long) (new Date().getTime() + 5.184e+8))) : "към " + new SimpleDateFormat("dd.MM.YYYY").format(new Date()));
        headCell2.setCellStyle(boldArial10Centered);
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 4));

        Row head3 = sheet.createRow(2);
        Cell headCell3 = head3.createCell(0);
        headCell3.setCellValue(isOverdue ? "(просрочени)" : "(неплатени)");
        headCell3.setCellStyle(italicArial10Centered);

        Font arialBold12 = workbook.createFont();
        arialBold12.setBold(true);
        arialBold12.setFontHeightInPoints((short) 10);
        arialBold12.setFontName("Arial");

        CellStyle topLeftCorner = workbook.createCellStyle();
        topLeftCorner.setBorderTop(BorderStyle.THICK);
        topLeftCorner.setBorderLeft(BorderStyle.THICK);
        topLeftCorner.setFont(arialBold12);

        CellStyle top = workbook.createCellStyle();
        top.setBorderTop(BorderStyle.THICK);
        top.setFont(arialBold12);

        CellStyle topRight = workbook.createCellStyle();
        topRight.setBorderTop(BorderStyle.THICK);
        topRight.setBorderRight(BorderStyle.THICK);
        topRight.setFont(arialBold12);

        CellStyle right = workbook.createCellStyle();
        right.setBorderRight(BorderStyle.THICK);
        right.setFont(arialBold12);

        CellStyle bottomRightCorner = workbook.createCellStyle();
        bottomRightCorner.setBorderRight(BorderStyle.THICK);
        bottomRightCorner.setBorderBottom(BorderStyle.THICK);
        bottomRightCorner.setFont(arialBold12);

        CellStyle bottom = workbook.createCellStyle();
        bottom.setBorderBottom(BorderStyle.THICK);
        bottom.setFont(arialBold12);

        CellStyle bottomLeftCorner = workbook.createCellStyle();
        bottomLeftCorner.setBorderBottom(BorderStyle.THICK);
        bottomLeftCorner.setBorderLeft(BorderStyle.THICK);
        bottomLeftCorner.setFont(arialBold12);

        CellStyle left = workbook.createCellStyle();
        left.setBorderLeft(BorderStyle.THICK);
        left.setFont(arialBold12);

        Row bigHeadRow1 = sheet.createRow(5);

        Cell head11 = bigHeadRow1.createCell(0);
        head11.setCellStyle(topLeftCorner);

        Cell head12 = bigHeadRow1.createCell(1);
        head12.setCellStyle(top);

        Cell head13 = bigHeadRow1.createCell(2);
        head13.setCellStyle(top);

        Cell head14 = bigHeadRow1.createCell(3);
        head14.setCellStyle(top);

        Cell head15 = bigHeadRow1.createCell(4);
        head15.setCellStyle(topRight);

        Row bigHeadRow2 = sheet.createRow(6);

        Cell head21 = bigHeadRow2.createCell(0);
        head21.setCellStyle(left);
        head21.setCellValue("НАЛИЧНОСТ към " + new SimpleDateFormat("dd.MM.YYYY").format(new Date()) + "г.");

        Cell head24 = bigHeadRow2.createCell(4);
        head24.setCellStyle(right);

        Row bigHeadRow3 = sheet.createRow(7);

        Cell head31 = bigHeadRow3.createCell(0);
        head31.setCellStyle(left);

        Cell head34 = bigHeadRow3.createCell(4);
        head34.setCellStyle(right);

        Row bigHeadRow4 = sheet.createRow(8);

        Cell head41 = bigHeadRow4.createCell(0);
        head41.setCellStyle(bottomLeftCorner);
        head41.setCellValue("Общо за справката:");

        Cell head42 = bigHeadRow4.createCell(1);
        head42.setCellStyle(bottom);
        head42.setCellValue(isOverdue ? oBgnSum : bgnSum);

        Cell head43 = bigHeadRow4.createCell(2);
        head43.setCellStyle(bottom);
        head43.setCellValue(isOverdue ? oEurSum : eurSum);

        Cell head44 = bigHeadRow4.createCell(3);
        head44.setCellStyle(bottom);
        head44.setCellValue(isOverdue ? oUsdSum : usdSum);

        Cell head45 = bigHeadRow4.createCell(4);
        head45.setCellStyle(bottomRightCorner);
    }

    private static void intiStyles(Workbook workbook) {
        boldArialFrame12 = workbook.createCellStyle();
        boldArialFrame12.setBorderBottom(BorderStyle.THIN);
        boldArialFrame12.setBorderLeft(BorderStyle.THIN);
        boldArialFrame12.setBorderRight(BorderStyle.THIN);
        boldArialFrame12.setBorderTop(BorderStyle.THIN);
        Font arialBold12 = workbook.createFont();
        arialBold12.setBold(true);
        arialBold12.setFontHeightInPoints((short) 12);
        arialBold12.setFontName("Arial");
        boldArialFrame12.setFont(arialBold12);

        boldArialFrame10 = workbook.createCellStyle();
        boldArialFrame10.setBorderBottom(BorderStyle.THIN);
        boldArialFrame10.setBorderLeft(BorderStyle.THIN);
        boldArialFrame10.setBorderRight(BorderStyle.THIN);
        boldArialFrame10.setBorderTop(BorderStyle.THIN);
        Font arialBold10 = workbook.createFont();
        arialBold10.setBold(true);
        arialBold10.setFontHeightInPoints((short) 10);
        arialBold10.setFontName("Arial");
        boldArialFrame10.setFont(arialBold10);

        leftArial10Thick = workbook.createCellStyle();
        leftArial10Thick.setBorderBottom(BorderStyle.THICK);
        leftArial10Thick.setBorderLeft(BorderStyle.THICK);
        leftArial10Thick.setBorderTop(BorderStyle.THICK);
        leftArial10Thick.setBorderRight(BorderStyle.THIN);
        leftArial10Thick.setFont(arialBold10);

        arial10Thick = workbook.createCellStyle();
        arial10Thick.setBorderBottom(BorderStyle.THICK);
        arial10Thick.setBorderRight(BorderStyle.THIN);
        arial10Thick.setBorderTop(BorderStyle.THICK);
        arial10Thick.setBorderLeft(BorderStyle.THIN);
        arial10Thick.setFont(arialBold10);

        righArial10Thick = workbook.createCellStyle();
        righArial10Thick.setBorderBottom(BorderStyle.THICK);
        righArial10Thick.setBorderRight(BorderStyle.THICK);
        righArial10Thick.setBorderTop(BorderStyle.THICK);
        righArial10Thick.setBorderLeft(BorderStyle.THIN);
        righArial10Thick.setFont(arialBold10);

        arialFrame10 = workbook.createCellStyle();
        arialFrame10.setBorderBottom(BorderStyle.THIN);
        arialFrame10.setBorderLeft(BorderStyle.THIN);
        arialFrame10.setBorderRight(BorderStyle.THIN);
        arialFrame10.setBorderTop(BorderStyle.THIN);
        Font arial10 = workbook.createFont();
        arial10.setFontHeightInPoints((short) 10);
        arial10.setFontName("Arial");
        arialFrame10.setFont(arial10);

        boldArial12 = workbook.createCellStyle();
        boldArial12.setFont(arialBold12);

        Font arialItalic10 = workbook.createFont();
        arialItalic10.setItalic(true);
        arialItalic10.setFontHeightInPoints((short) 10);

        boldArial12Centered = workbook.createCellStyle();
        boldArial12Centered.setFont(arialBold12);
        boldArial12Centered.setAlignment(HorizontalAlignment.CENTER);

        boldArial10Centered = workbook.createCellStyle();
        boldArial10Centered.setFont(arialBold10);
        boldArial10Centered.setAlignment(HorizontalAlignment.CENTER);

        italicArial10Centered = workbook.createCellStyle();
        italicArial10Centered.setFont(arialItalic10);
        italicArial10Centered.setAlignment(HorizontalAlignment.CENTER);

    }
}

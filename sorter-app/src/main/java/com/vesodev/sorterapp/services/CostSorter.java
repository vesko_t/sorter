package com.vesodev.sorterapp.services;

import com.vesodev.sorterapp.model.Comment;
import com.vesodev.sorterapp.model.Invoice;
import com.vesodev.sorterapp.model.SortingGroup;
import com.vesodev.sorterapp.utils.SorterUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CostSorter {

    private CostSorter() {
    }

    public static void sort(File file, int days) throws Exception {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(file);

            List<SortingGroup> sortingGroups = ServerConnector.getGroups();
            List<Comment> comments = ServerConnector.getComments();

            List<Invoice> invoices = CostExtractor.extractData(workbook, sortingGroups, comments);

            if (sortingGroups != null) {
                for (SortingGroup sortingGroup : sortingGroups) {
                    if (sortingGroup.getSortingDataList().size() != 0) {
                        List<Invoice> groupInvoices = SorterUtils.getGroupInvoices(sortingGroup, invoices);
                        Workbook groupWorkbook = new XSSFWorkbook();
                        if (groupInvoices.size() != 0) {
                            createInvoiceSheets(groupWorkbook, sortingGroup, groupInvoices, days);
                            saveFile(groupWorkbook, file, sortingGroup.getGroupName());
                        }
                    }
                }
            }

            createInvoiceSheets(workbook, null, invoices, days);

            saveFile(workbook, file, "");

            JOptionPane.showMessageDialog(null, "Справката е генерирана успешно!", "Успешно запазен файл", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            if (workbook != null) {
                workbook.close();
            }
            throw e;
        }
    }


    /**
     * Creates a sorted and formated workbook sheet from the provided invoices and sorts them using the sorting gorups
     *
     * @param workbook
     * @param sortingGroup
     * @param invoices
     * @param days
     */
    private static void createInvoiceSheets(Workbook workbook, SortingGroup sortingGroup, List<Invoice> invoices, int days) {
        Map<String, List<Invoice>> sortedCurrency = sortByCurrency(invoices);

        sortedCurrency.keySet().forEach((key) -> {
            IncomeSorter.sortAlphabetically(sortedCurrency.get(key));
        });

        Map<String, List<List<Invoice>>> unpaidSorted = separate(sortedCurrency);

        unpaidSorted.forEach((key, currency) -> {
            for (int i = 0; i < currency.size(); i++) {
                currency.set(i, sortByPayDateAndNumber(currency.get(i)));
            }
        });

        CostFormater.formatAndCreateSheet(unpaidSorted, workbook,
                sortingGroup == null ? IncomeSorter.unpaidName : sortingGroup.getGroupName() + " - " + IncomeSorter.unpaidName,
                sortingGroup == null ? IncomeSorter.overdueName : sortingGroup.getGroupName() + " - " + IncomeSorter.overdueName, days);
    }

    private static List<Invoice> sortByPayDateAndNumber(List<Invoice> invoices) {
        invoices.sort(Comparator.comparing(Invoice::getNumber));

        List<Invoice> combined = combineInvoices(invoices);

        combined.sort((o1, o2) -> {
            Date x1 = o1.getPayDate();
            Date x2 = o2.getPayDate();
            int sComp = x1.compareTo(x2);

            if (sComp != 0) {
                return sComp;
            }

            String y1 = o1.getNumber();
            String y2 = o2.getNumber();
            return y1.compareTo(y2);
        });
        return combined;
    }


    private static Map<String, List<List<Invoice>>> separate(Map<String, List<Invoice>> currInvoices) {
        Map<String, List<List<Invoice>>> sorted = new HashMap<>();
        final List<List<Invoice>>[] currencyInvoices = new List[]{new ArrayList<>()};
        final String[] invoiceName = {""};
        currInvoices.keySet().forEach((key) -> {
            currencyInvoices[0] = new ArrayList<>();
            final List<Invoice>[] alphabeticInvoices = new List[]{new ArrayList<>()};
            currInvoices.get(key).forEach(invoice -> {
                if (invoice.getClientName().equals(invoiceName[0])) {
                    alphabeticInvoices[0].add(invoice);
                } else {
                    alphabeticInvoices[0] = new ArrayList<>();
                    alphabeticInvoices[0].add(invoice);
                    invoiceName[0] = invoice.getClientName();
                    currencyInvoices[0].add(alphabeticInvoices[0]);
                }
            });
            sorted.put(key, currencyInvoices[0]);
        });

        return sorted;
    }

    private static List<Invoice> combineInvoices(List<Invoice> invoices) {
        List<Invoice> combined = new ArrayList<>();

        Invoice template = new Invoice(invoices.get(0));
        //invoices.remove(template);
        for (int i = 1; i < invoices.size(); i++) {
            Invoice invoice = invoices.get(i);
            if (invoice.equals(template)) {
                template.setAmountLeft(template.getAmountLeft() + invoice.getAmountLeft());
            } else {
                combined.add(template);
                template = new Invoice(invoice);
            }
        }
        combined.add(template);

        return combined;
    }

    private static Map<String, List<Invoice>> sortByCurrency(List<Invoice> invoices) {
        List<Invoice> bgn = new ArrayList<>();
        List<Invoice> usd = new ArrayList<>();
        List<Invoice> euro = new ArrayList<>();
        Map<String, List<Invoice>> invoiceList = new HashMap<>();
        for (Invoice invoice : invoices) {
            switch (invoice.getCurrency()) {
                case Invoice.BGNOther:
                    bgn.add(invoice);
                    break;
                case Invoice.USD:
                    usd.add(invoice);
                    break;
                case Invoice.EUR:
                    euro.add(invoice);
                    break;
            }
        }
        invoiceList.put(Invoice.USD, usd);
        invoiceList.put(Invoice.EUR, euro);
        invoiceList.put(Invoice.BGNOther, bgn);
        return invoiceList;
    }

    private static void saveFile(Workbook workbook, File parentFile, String suffix) throws IOException {
        File saveFile = new File(parentFile.getParentFile().getAbsolutePath() + "/Справка задължения към " + new SimpleDateFormat("dd.MM.YYYY").format(new Date()) + suffix + ".xlsx");
        if (!saveFile.exists()) {
            saveFile.createNewFile();
        }
        FileOutputStream outputStream = new FileOutputStream(saveFile);
        workbook.write(outputStream);
        outputStream.close();
    }
}

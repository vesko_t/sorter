package com.vesodev.sorterapp.services;

import com.vesodev.sorterapp.model.*;
import com.vesodev.sorterapp.utils.SorterUtils;
import com.vesodev.sorterapp.model.Invoice;
import com.vesodev.sorterapp.model.SortingGroup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Aossia on 2/21/2020.
 */
public final class IncomeExtractor {

    private IncomeExtractor() {

    }

    public static List<Invoice> extractData(Workbook workbook, List<SortingGroup> sortingGroups, List<Comment> comments) {
        List<Invoice> invoices = new ArrayList<Invoice>();
        List<Comment> numberComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.NUMBER.equals(comment.getProperties()) && comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();
        List<Comment> issuerComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.ISSUER.equals(comment.getProperties()) && comment.getIncome()).collect(Collectors.toList()) :new ArrayList<>();
        List<Comment> clientComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.CLIENT_NAME.equals(comment.getProperties()) && comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();
        List<Comment> amountComments = comments != null ? comments.stream().filter(comment -> InvoiceProperties.AMOUNT_LEFT.equals(comment.getProperties()) && comment.getIncome()).collect(Collectors.toList()) : new ArrayList<>();

        int i = 0;
        for (Row row : workbook.getSheet("inv")) {
            if (i > 0) {
                Invoice invoice = new Invoice();
                //Client name
                if (row.getCell(2) != null) {
                    if (row.getCell(2).getStringCellValue() != null) {
                        invoice.setClientName(row.getCell(2).getStringCellValue());
                        for (Comment comment : clientComments) {
                            if (SorterUtils.areEqual(invoice.getClientName(),comment.getProperty())) {
                                invoice.appendComment(comment.getValue());
                            }
                        }
                    }
                }
                //Number
                if (row.getCell(3) != null) {
                    if (row.getCell(3).getStringCellValue() != null) {
                        invoice.setId(Integer.parseInt(row.getCell(3).getStringCellValue().replaceAll("[^\\d.]", "")));
                        for (Comment comment : numberComments) {
                            if (String.valueOf(invoice.getId()).equals(comment.getProperty())) {
                                invoice.appendComment(comment.getValue());
                            }
                        }
                    }
                }
                //Issue date
                if (row.getCell(4) != null) {
                    if (row.getCell(4).getDateCellValue() != null) {
                        invoice.setIssueDate(row.getCell(4).getDateCellValue());
                    }
                }
                //Amount left
                if (row.getCell(24) != null) {
                    if (row.getCell(24).getNumericCellValue() != 0.0) {
                        invoice.setAmountLeft(row.getCell(24).getNumericCellValue());
                        for (Comment comment : amountComments) {
                            if (String.valueOf(invoice.getAmountLeft()).equals(comment.getProperty())) {
                                invoice.appendComment(comment.getValue());
                            }
                        }
                    }
                }
                //Currency
                if (row.getCell(10) != null) {
                    if (row.getCell(10).getStringCellValue() != null) {
                        invoice.setCurrency(row.getCell(10).getStringCellValue());
                    }
                }
                //Issuer
                if (row.getCell(15) != null) {
                    if (row.getCell(15).getStringCellValue() != null) {
                        invoice.setIssuer(row.getCell(15).getStringCellValue());
                        for (Comment comment : issuerComments) {
                            if (invoice.getIssuer().replaceAll("\\s+", "").equals(comment.getProperty().replaceAll("\\s+", ""))) {
                                invoice.appendComment(comment.getValue());
                            }
                        }
                    }
                }
                //Pay Date
                if (row.getCell(11) != null) {
                    if (row.getCell(11).getDateCellValue() != null) {
                        invoice.setPayDate(row.getCell(11).getDateCellValue());
                    }
                }
                if (sortingGroups != null) {
                    SorterUtils.extractCustomData(sortingGroups, row, invoice);
                }
                if (invoice.getCurrency() != null &&
                        invoice.getAmountLeft() != 0.0 &&
                        invoice.getClientName() != null &&
                        invoice.getIssuer() != null &&
                        invoice.getIssueDate() != null &&
                        invoice.getId() != 0)
                    invoices.add(invoice);
            }
            i++;
        }
        return invoices;
    }


}

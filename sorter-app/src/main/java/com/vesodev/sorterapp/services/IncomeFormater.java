package com.vesodev.sorterapp.services;

import com.vesodev.sorterapp.model.Invoice;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Aossia on 2/21/2020.
 */
public class IncomeFormater {


    private static CellStyle boldArialFrame12;
    private static CellStyle boldArialFrame10;
    private static CellStyle boldArial12;
    private static CellStyle arialFrame10;
    private static CellStyle arialFrame10Red;
    private static CellStyle boldArial12Centered;
    private static CellStyle boldArial10Centered;
    private static CellStyle italicArial10Centered;
    private static CellStyle boldArialFrame12Centerd;

    static final String unpaidName = "Неплатени";
    static final String overdueName = "Просрочени";

    private static double bgnSum = 0;
    private static double eurSum = 0;
    private static double usdSum = 0;
    private static double gbpSum = 0;

    public static void formatAndCreateSheet(List<List<Invoice>> invoiceBlocks, Workbook workbook, String sheetName, boolean isOverdue) {
        intiStyles(workbook);
        if (boldArial10Centered != null) {
            bgnSum = 0;
            eurSum = 0;
            usdSum = 0;
            gbpSum = 0;
        }

        setSums(invoiceBlocks);

        generateSheet(invoiceBlocks, workbook, sheetName, isOverdue);

    }

    private static void generateSheet(List<List<Invoice>> invoiceBlocks, Workbook workbook, String sheetName, boolean isOverdue) {
        Sheet sheet = workbook.createSheet(sheetName);
        setHeader(sheet, isOverdue);
        int totalRows = 5;
        for (List<Invoice> invoices : invoiceBlocks) {
            int num = 0;
            String invoiceCurrency = "";
            for (int i = 0; i < invoices.size(); i++) {
                Invoice invoice = invoices.get(i);
                Row row = sheet.createRow(totalRows);

                invoiceCurrency = invoice.getCurrency();

                fillRow(row, invoice, invoices.size() == 1);
                totalRows++;
                num++;
            }
            if (num > 1) {
                Row row = sheet.createRow(totalRows);
                Cell cell = row.createCell(2);
                cell.setCellValue("Общо:");
                cell.setCellStyle(boldArialFrame10);
                row.createCell(3).setCellStyle(boldArialFrame10);
                Cell total = row.createCell(4);
                total.setCellType(CellType.FORMULA);
                total.setCellFormula("SUM(" +
                        sheet.getRow(totalRows - 1).getCell(4).getAddress() + ":" +
                        sheet.getRow(totalRows - num).getCell(4).getAddress() + ")");
                total.setCellStyle(boldArialFrame10);
                sheet.addMergedRegion(new CellRangeAddress(totalRows, totalRows, 2, 3));

                Cell totalCurrency = row.createCell(5);
                totalCurrency.setCellValue(invoiceCurrency);
                totalCurrency.setCellStyle(boldArialFrame10);

                totalRows++;
            }

            totalRows++;
        }
        setTotals(sheet, totalRows);
    }

    private static void intiStyles(Workbook workbook) {
        boldArialFrame12 = workbook.createCellStyle();
        boldArialFrame12.setBorderBottom(BorderStyle.THIN);
        boldArialFrame12.setBorderLeft(BorderStyle.THIN);
        boldArialFrame12.setBorderRight(BorderStyle.THIN);
        boldArialFrame12.setBorderTop(BorderStyle.THIN);
        Font arialBold12 = workbook.createFont();
        arialBold12.setBold(true);
        arialBold12.setFontHeightInPoints((short) 12);
        arialBold12.setFontName("Arial");
        boldArialFrame12.setFont(arialBold12);

        boldArialFrame12Centerd = workbook.createCellStyle();
        boldArialFrame12Centerd.setBorderBottom(BorderStyle.THIN);
        boldArialFrame12Centerd.setBorderLeft(BorderStyle.THIN);
        boldArialFrame12Centerd.setBorderRight(BorderStyle.THIN);
        boldArialFrame12Centerd.setBorderTop(BorderStyle.THIN);
        arialBold12.setBold(true);
        arialBold12.setFontHeightInPoints((short) 12);
        arialBold12.setFontName("Arial");
        boldArialFrame12Centerd.setFont(arialBold12);
        boldArialFrame12Centerd.setAlignment(HorizontalAlignment.CENTER);


        boldArialFrame10 = workbook.createCellStyle();
        boldArialFrame10.setBorderBottom(BorderStyle.THIN);
        boldArialFrame10.setBorderLeft(BorderStyle.THIN);
        boldArialFrame10.setBorderRight(BorderStyle.THIN);
        boldArialFrame10.setBorderTop(BorderStyle.THIN);
        Font arialBold10 = workbook.createFont();
        arialBold10.setBold(true);
        arialBold10.setFontHeightInPoints((short) 10);
        arialBold10.setFontName("Arial");
        boldArialFrame10.setFont(arialBold10);

        arialFrame10 = workbook.createCellStyle();
        arialFrame10.setBorderBottom(BorderStyle.THIN);
        arialFrame10.setBorderLeft(BorderStyle.THIN);
        arialFrame10.setBorderRight(BorderStyle.THIN);
        arialFrame10.setBorderTop(BorderStyle.THIN);
        Font arial10 = workbook.createFont();
        arial10.setFontHeightInPoints((short) 10);
        arial10.setFontName("Arial");
        arialFrame10.setFont(arial10);

        arialFrame10Red = workbook.createCellStyle();
        arialFrame10Red.setBorderBottom(BorderStyle.THIN);
        arialFrame10Red.setBorderLeft(BorderStyle.THIN);
        arialFrame10Red.setBorderRight(BorderStyle.THIN);
        arialFrame10Red.setBorderTop(BorderStyle.THIN);
        Font arial10Red = workbook.createFont();
        arial10Red.setFontHeightInPoints((short) 10);
        arial10Red.setFontName("Arial");
        arial10Red.setColor(IndexedColors.RED.getIndex());
        /*arialFrame10Red.setFillForegroundColor(IndexedColors.RED.getIndex());
        arialFrame10Red.setFillPattern(FillPatternType.SOLID_FOREGROUND);*/
        arialFrame10Red.setFont(arial10Red);

        boldArial12 = workbook.createCellStyle();
        boldArial12.setFont(arialBold12);

        Font arialItalic10 = workbook.createFont();
        arialItalic10.setItalic(true);
        arialItalic10.setFontHeightInPoints((short) 10);

        boldArial12Centered = workbook.createCellStyle();
        boldArial12Centered.setFont(arialBold12);
        boldArial12Centered.setAlignment(HorizontalAlignment.CENTER);

        boldArial10Centered = workbook.createCellStyle();
        boldArial10Centered.setFont(arialBold10);
        boldArial10Centered.setAlignment(HorizontalAlignment.CENTER);

        italicArial10Centered = workbook.createCellStyle();
        italicArial10Centered.setFont(arialItalic10);
        italicArial10Centered.setAlignment(HorizontalAlignment.CENTER);
    }

    private static void setHeader(Sheet sheet, boolean isOverdue) {
        Row head = sheet.createRow(0);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
        Cell headCell = head.createCell(0);
        headCell.setCellValue("Справка Вземания");
        headCell.setCellStyle(boldArial12Centered);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 6));

        Row head2 = sheet.createRow(1);
        Cell headCell2 = head2.createCell(0);
        headCell2.setCellValue("към " + new SimpleDateFormat("dd.MM.YYYY").format(new Date()));
        headCell2.setCellStyle(boldArial10Centered);
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 6));

        Row head3 = sheet.createRow(2);
        Cell headCell3 = head3.createCell(0);
        headCell3.setCellValue(isOverdue ? "(просрочени)" : "(неплатени)");
        headCell3.setCellStyle(italicArial10Centered);


        Row infoRow = sheet.createRow(4);
        Cell name = infoRow.createCell(0);
        name.setCellValue("Клиент");
        name.setCellStyle(boldArialFrame10);

        Cell id = infoRow.createCell(1);
        id.setCellValue("Фактура");
        id.setCellStyle(boldArialFrame10);

        Cell date = infoRow.createCell(2);
        date.setCellValue("Дата");
        date.setCellStyle(boldArialFrame10);

        Cell overdue = infoRow.createCell(3);
        overdue.setCellValue("Просрочка");
        overdue.setCellStyle(boldArialFrame10);

        Cell ammount = infoRow.createCell(4);
        ammount.setCellValue("Остатък");
        ammount.setCellStyle(boldArialFrame10);

        Cell currency = infoRow.createCell(5);
        currency.setCellValue("Валута");
        currency.setCellStyle(boldArialFrame10);

        Cell issuer = infoRow.createCell(6);
        issuer.setCellValue("Съставил");
        issuer.setCellStyle(boldArialFrame10);
    }

    private static void setTotals(Sheet sheet, int totalRows) {
        if (eurSum > 0.0) {
            totalRows++;
            Row rowEUR = sheet.createRow(totalRows);
            sheet.addMergedRegion(new CellRangeAddress(totalRows, totalRows, 2, 3));
            Cell eurTotal = rowEUR.createCell(2);
            eurTotal.setCellValue("ТОТАЛ:");
            eurTotal.setCellStyle(boldArialFrame12Centerd);
            Cell eurSumCell = rowEUR.createCell(4);
            eurSumCell.setCellValue(Math.round(eurSum));
            eurSumCell.setCellStyle(boldArialFrame12);
            Cell eurCur = rowEUR.createCell(5);
            eurCur.setCellValue(Invoice.EUR);
            eurCur.setCellStyle(boldArialFrame12);
        }
        if (usdSum > 0.0) {
            totalRows++;
            Row rowUSD = sheet.createRow(totalRows);
            sheet.addMergedRegion(new CellRangeAddress(totalRows, totalRows, 2, 3));
            Cell usdTotal = rowUSD.createCell(2);
            usdTotal.setCellValue("ТОТАЛ:");
            usdTotal.setCellStyle(boldArialFrame12Centerd);
            Cell usdSumCell = rowUSD.createCell(4);
            usdSumCell.setCellValue(Math.round(usdSum));
            usdSumCell.setCellStyle(boldArialFrame12);
            Cell usdCur = rowUSD.createCell(5);
            usdCur.setCellValue(Invoice.USD);
            usdCur.setCellStyle(boldArialFrame12);
        }
        if (bgnSum > 0.0) {
            totalRows++;
            Row rowBGN = sheet.createRow(totalRows);
            sheet.addMergedRegion(new CellRangeAddress(totalRows, totalRows, 2, 3));
            Cell bgnTotal = rowBGN.createCell(2);
            bgnTotal.setCellValue("ТОТАЛ:");
            bgnTotal.setCellStyle(boldArialFrame12Centerd);
            Cell bgnSumCell = rowBGN.createCell(4);
            bgnSumCell.setCellValue(Math.round(bgnSum));
            bgnSumCell.setCellStyle(boldArialFrame12);
            Cell bgnCur = rowBGN.createCell(5);
            bgnCur.setCellValue(Invoice.BGN);
            bgnCur.setCellStyle(boldArialFrame12);
        }
        if (gbpSum > 0.0) {
            totalRows++;
            Row rowGBP = sheet.createRow(totalRows);
            sheet.addMergedRegion(new CellRangeAddress(totalRows, totalRows, 2, 3));
            Cell gbpTotal = rowGBP.createCell(2);
            gbpTotal.setCellValue("ТОТАЛ:");
            gbpTotal.setCellStyle(boldArialFrame12Centerd);
            Cell gbpSumCell = rowGBP.createCell(4);
            gbpSumCell.setCellValue(Math.round(gbpSum));
            gbpSumCell.setCellStyle(boldArialFrame12);
            Cell gbpCur = rowGBP.createCell(5);
            gbpCur.setCellValue(Invoice.GBP);
            gbpCur.setCellStyle(boldArialFrame12);
        }
    }

    private static void fillRow(Row row, Invoice invoice, boolean isTheOnlyOne) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        Cell name = row.createCell(0);
        name.setCellValue(invoice.getClientName());
        name.setCellStyle(arialFrame10);

        Cell id = row.createCell(1);
        id.setCellValue(invoice.getId());
        id.setCellStyle(arialFrame10);

        Cell date = row.createCell(2);
        date.setCellValue(dateFormat.format(invoice.getIssueDate()));
        date.setCellStyle(arialFrame10);

        Cell overdue = row.createCell(3);
        long diff = new Date().getTime() - invoice.getPayDate().getTime();
        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        overdue.setCellValue(days);
        if (days >= 0) {
            overdue.setCellStyle(arialFrame10Red);
        } else {
            overdue.setCellStyle(arialFrame10);
        }
        Cell amount = row.createCell(4);
        amount.setCellValue(invoice.getAmountLeft());
        amount.setCellType(CellType.NUMERIC);
        amount.setCellStyle(isTheOnlyOne ? boldArialFrame10 : arialFrame10);

        Cell currency = row.createCell(5);
        currency.setCellValue(invoice.getCurrency());
        currency.setCellStyle(isTheOnlyOne ? boldArialFrame10 : arialFrame10);

        Cell issuer = row.createCell(6);
        issuer.setCellValue(invoice.getIssuer());
        issuer.setCellStyle(arialFrame10);

        if (invoice.getComment() != null && !"".equals(invoice.getComment())) {
            Cell comment = row.createCell(7);
            comment.setCellValue(invoice.getComment());
        }
    }

    private static void setSums(List<List<Invoice>> invoiceBlocks) {
        for (List<Invoice> invoices : invoiceBlocks) {
            for (Invoice invoice : invoices) {
                if (invoice.getCurrency().equals(Invoice.BGN) || invoice.getCurrency().equals(Invoice.BGNOther)) {
                    bgnSum += invoice.getAmountLeft();
                } else if (invoice.getCurrency().equals(Invoice.EUR)) {
                    eurSum += invoice.getAmountLeft();

                } else if (invoice.getCurrency().equals(Invoice.GBP)) {
                    gbpSum += invoice.getAmountLeft();


                } else if (invoice.getCurrency().equals(Invoice.USD)) {

                    usdSum += invoice.getAmountLeft();

                }
            }
        }
    }

}

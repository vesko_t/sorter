package com.vesodev.sorterapp.services;

import com.vesodev.sorterapp.model.Comment;
import com.vesodev.sorterapp.model.Invoice;
import com.vesodev.sorterapp.model.SortingGroup;
import com.vesodev.sorterapp.utils.SorterUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Vesko on 4.3.2018 г..
 */
public class IncomeSorter {

    /*
     * D:\Справка 15.05.2018.xlsx
     * */

    static final String unpaidName = "Неплатени";
    static final String overdueName = "Просрочени";

    private IncomeSorter() {
    }

    /**
     * Creates a workbook from file
     *
     * @param file
     * @param modifyPayDate
     */
    public static void sort(File file, boolean modifyPayDate) throws Exception {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(file);
            Workbook collectedWorkbook = new XSSFWorkbook();

            List<SortingGroup> sortingGroups = ServerConnector.getGroups();

            List<Comment> comments = ServerConnector.getComments();

            List<Invoice> invoices = IncomeExtractor.extractData(workbook, sortingGroups, comments);

            if (sortingGroups != null) {
                for (SortingGroup sortingGroup : sortingGroups) {
                    if (sortingGroup.getSortingDataList().size() != 0) {
                        List<Invoice> groupInvoices = SorterUtils.getGroupInvoices(sortingGroup, invoices);
                        Workbook groupWorkbook = new XSSFWorkbook();
                        if (groupInvoices.size() != 0) {
                            createInvoiceSheets(groupWorkbook, sortingGroup, groupInvoices, modifyPayDate);
                            saveFile(groupWorkbook, file, sortingGroup.getGroupName());
                        }
                    }
                }
            }

            createInvoiceSheets(workbook, null, invoices, modifyPayDate);
            saveFile(workbook, file, "");
            JOptionPane.showMessageDialog(null, "Справката е генерирана успешно!", "Успешно запазен файл", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            if (workbook != null) {
                workbook.close();
            }
            throw e;
        }
    }


    /**
     * Creates a sorted and formated workbook sheet from the provided invoices and sorts them using the sorting gorups
     *
     * @param workbook
     * @param sortingGroup
     * @param invoices
     * @param modifyPayDate
     */
    private static void createInvoiceSheets(Workbook workbook, SortingGroup sortingGroup, List<Invoice> invoices, boolean modifyPayDate) {
        invoices = sortAlphabetically(invoices);

        List<List<Invoice>> invoiceBlocks = generateInvoiceBlocks(invoices);

        List<List<Invoice>> invoiceBlocksOverdue = generateInvoiceBlocksOverdue(invoices, modifyPayDate);

        IncomeFormater.formatAndCreateSheet(invoiceBlocks, workbook, sortingGroup == null ? unpaidName : sortingGroup.getGroupName() + " - " + unpaidName, false);
        IncomeFormater.formatAndCreateSheet(invoiceBlocksOverdue, workbook, sortingGroup == null ? overdueName : sortingGroup.getGroupName() + " - " + overdueName, true);
    }

    static List<Invoice> sortAlphabetically(List<Invoice> invoices) {
        Collections.sort(invoices, new Comparator<Invoice>() {
            public int compare(final Invoice object1, final Invoice object2) {
                return object1.getClientName().compareTo(object2.getClientName());
            }
        });
        return invoices;
    }

    private static void printInvoices(List<Invoice> invoices) {
        for (Invoice invoice : invoices) {
            System.out.println(invoice.getClientName() + "\t" +
                    invoice.getId() + "\t" +
                    invoice.getAmountLeft() + "\t" +
                    invoice.getCurrency() +
                    "\t" + invoice.getIssueDate() + "\t" +
                    invoice.getIssuer());
        }
    }

    private static List<List<Invoice>> generateInvoiceBlocks(List<Invoice> invoices) {
        List<List<Invoice>> invoiceBlocks = new ArrayList<List<Invoice>>();
        List<Invoice> invoiceBlock = new ArrayList<Invoice>();
        String company = invoices.get(0).getClientName();
        for (Invoice invoice : invoices) {
            if (!invoice.getClientName().equals(company)) {
                company = invoice.getClientName();
                invoiceBlocks.add(invoiceBlock);
                invoiceBlock = new ArrayList<Invoice>();
                invoiceBlock.add(invoice);
            } else if (invoice.getClientName().equals(company)) {
                invoiceBlock.add(invoice);
            }
        }
        invoiceBlocks.add(invoiceBlock);
        return invoiceBlocks;
    }

    public static List<List<Invoice>> generateInvoiceBlocksOverdue(List<Invoice> invoices, boolean modifyPayDate) {
        List<List<Invoice>> invoiceBlocks = new ArrayList<List<Invoice>>();
        List<Invoice> invoiceBlock = new ArrayList<Invoice>();
        String company = invoices.get(0).getClientName();
        for (Invoice invoice : invoices) {
            if (!invoice.getClientName().equals(company)) {
                company = invoice.getClientName();
                if (invoiceBlock.size() > 0) {
                    invoiceBlocks.add(invoiceBlock);
                    invoiceBlock = new ArrayList<Invoice>();
                }
                if (isOverdue(invoice, modifyPayDate)) {
                    invoiceBlock.add(invoice);
                }
            } else {
                if (isOverdue(invoice, modifyPayDate)) {
                    invoiceBlock.add(invoice);
                }
            }
        }
        invoiceBlocks.add(invoiceBlock);
        return invoiceBlocks;
    }

    private static boolean isOverdue(Invoice invoice, boolean modifyPaydate) {
        if (invoice.getPayDate() == null) {
            invoice.setPayDate(invoice.getIssueDate());
        }
        if (invoice.getPayDate().equals(invoice.getIssueDate()) && modifyPaydate) {
            invoice.setPayDate(new Date(invoice.getPayDate().getTime() + 2592000000L));
        }
        if (invoice.getPayDate().getTime() < System.currentTimeMillis()) {
            return true;
        } else return false;
    }

    private static void saveFile(Workbook workbook, File parentFile, String suffix) throws IOException {
        File saveFile = new File(parentFile.getParentFile().getAbsolutePath() + "/Справка вземания към " + new SimpleDateFormat("dd.MM.YYYY").format(new Date()) + suffix + ".xlsx");
        if (!saveFile.exists()) {
            saveFile.createNewFile();
        }
        FileOutputStream outputStream = new FileOutputStream(saveFile);
        workbook.write(outputStream);
        outputStream.close();
    }
}

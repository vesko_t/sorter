package com.vesodev.sorterapp.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.vesodev.sorterapp.model.Comment;
import com.vesodev.sorterapp.model.SortingGroup;
import com.vesodev.sorterapp.utils.SorterUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vesko on 25.02.20.
 */
public class ServerConnector implements Callback {

    private static List<SortingGroup> sortingGroups;

    private static List<Comment> comments;

    private static OkHttpClient httpClient;

    private static boolean groupsInit = false;

    private static boolean commentsInit = false;

    private final String data;

    public ServerConnector(String data) {
        this.data = data;
    }

    public static void init() {
        httpClient = new OkHttpClient();
        try {
            getSortingGroups();
        } catch (IOException ignored) {
            SorterUtils.sendError("Грешка", "Не могат да бъдат получени данни от сървър!", null);
        }
    }

    private static void getSortingGroups() throws IOException {
        Request groupsRequest = new Request.Builder()
                .url(SorterUtils.getServerIp().toString() + "/get-groups")
                .build();
        Request commentRequest = new Request.Builder()
                .url(SorterUtils.getServerIp().toString() + "/comments")
                .build();
        httpClient.newCall(groupsRequest).enqueue(new ServerConnector("groups"));
        httpClient.newCall(commentRequest).enqueue(new ServerConnector("comments"));
    }

    public static List<SortingGroup> getGroups() {
        return sortingGroups;
    }

    public static List<Comment> getComments() {
        return comments;
    }

    @Override
    public void onFailure(Request request, IOException e) {
        SorterUtils.sendError("Грешка", "Не може да бъде установена връзка със сървъра", null);
    }

    @Override
    public void onResponse(Response response) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        if (data.equals("groups")) {
            SortingGroup[] sortingGroupsArray = objectMapper.readValue(response.body().string(), SortingGroup[].class);
            sortingGroups = Arrays.asList(sortingGroupsArray);
            groupsInit = true;
        }else if(data.equals("comments")){
            Comment[] commentsArray = objectMapper.readValue(response.body().string(), Comment[].class);
            comments = Arrays.asList(commentsArray);
            groupsInit = true;
        }
    }

    public static boolean isInitialized() {
        return groupsInit && commentsInit;
    }
}

package com.vesodev.sorterapp.utils;

public class CostData {

    private String directory;

    private int daysPeriod;

    public CostData() {
    }

    public CostData(String directory, int daysPeriod) {
        this.directory = directory;
        this.daysPeriod = daysPeriod;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public int getDaysPeriod() {
        return daysPeriod;
    }

    public void setDaysPeriod(int daysPeriod) {
        this.daysPeriod = daysPeriod;
    }
}

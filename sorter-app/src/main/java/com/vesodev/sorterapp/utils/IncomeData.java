package com.vesodev.sorterapp.utils;

public class IncomeData {

    private String directory;

    private boolean modifyPayDate;

    public IncomeData() {
    }

    public IncomeData(String directory, boolean modifyPayDate) {
        this.directory = directory;
        this.modifyPayDate = modifyPayDate;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public boolean isModifyPayDate() {
        return modifyPayDate;
    }

    public void setModifyPayDate(boolean modifyPayDate) {
        this.modifyPayDate = modifyPayDate;
    }
}

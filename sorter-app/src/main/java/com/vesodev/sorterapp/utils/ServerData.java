package com.vesodev.sorterapp.utils;

/**
 * Created by vesko on 09.03.20.
 */
public class ServerData {
    private String ip;
    private String port;

    public ServerData(String ip, String port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "http://" + ip + ":" + port + "/api";
    }
}

package com.vesodev.sorterapp.utils;

import com.vesodev.sorterapp.model.*;
import com.vesodev.sorterapp.model.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SorterUtils {

    private SorterUtils() {
    }

    public static void saveIncomeData(File file, boolean modifyPayDate) {
        File saveFile = new File("./incomeDirectory");
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (PrintWriter printWriter = new PrintWriter(saveFile)) {
            printWriter.print(file.getParentFile() + ", " + modifyPayDate);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void saveCostDirectory(File file, int days) {
        File saveFile = new File("./costDirectory");
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (PrintWriter printWriter = new PrintWriter(saveFile);) {
            printWriter.print(file.getParentFile() + ", " + days);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static IncomeData loadIncomeData() {
        File saveFile = new File("./incomeDirectory");
        IncomeData data = new IncomeData();
        if (saveFile.exists()) {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(saveFile))) {
                String output = bufferedReader.readLine();
                bufferedReader.close();
                if (output != null && output.equals("")) {
                    return null;
                } else {
                    String[] outputSplit = output.split(", ");
                    data.setDirectory(outputSplit[0]);
                    if (outputSplit.length > 1) {
                        data.setModifyPayDate(new Boolean(outputSplit[1]));
                    } else {
                        data.setModifyPayDate(true);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else return null;
        return data;
    }

    public static CostData loadCostData() {
        File saveFile = new File("./costDirectory");
        CostData data = new CostData();
        if (saveFile.exists()) {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(saveFile))) {
                String output = bufferedReader.readLine();
                bufferedReader.close();
                if (output != null && output.equals("")) {
                    return null;
                } else {
                    String[] outputSplit = output.split(", ");
                    data.setDirectory(outputSplit[0]);
                    if (outputSplit.length > 1) {
                        data.setDaysPeriod(new Integer(outputSplit[1]));
                    } else {
                        data.setDaysPeriod(7);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else return null;
        return data;
    }

    public static String loadDoc(boolean isIncome) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(SorterUtils.class.getResourceAsStream(isIncome ? "/incomeDoc.txt" : "/costDoc.txt"), "UTF8"))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
            bufferedReader.close();
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sendError(String title, String message, Component parent) {
        JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static void extractCustomData(List<SortingGroup> sortingGroups, Row row, Invoice invoice) {
        //star extracting custom data
        for (SortingGroup sortingGroup : sortingGroups) {
            for (SortingData sortingData : sortingGroup.getSortingDataList()) {
                if (sortingData.getInvoiceProperties() == null && sortingData.getCollNumber() != null) {
                    Cell cell = row.getCell(sortingData.getCollNumber());
                    if (cell != null) {
                        if (!invoice.getAdditionalValues().containsKey(sortingData.getCollNumber())) {
                            if (sortingData.isDate()) {
                                invoice.getAdditionalValues().put(sortingData.getCollNumber(), new ColumnValue(cell.getDateCellValue()));
                            } else if (sortingData.isNumber()) {
                                invoice.getAdditionalValues().put(sortingData.getCollNumber(), new ColumnValue(cell.getNumericCellValue()));
                            } else if (sortingData.isString()) {
                                invoice.getAdditionalValues().put(sortingData.getCollNumber(), new ColumnValue(cell.getStringCellValue()));
                            }
                        }
                    }
                }
            }
        }
        //end extracting custom data
    }

    public static void saveServerInfo(String ip, String port) {
        File saveFile = new File("./serverConfig");
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (PrintWriter printWriter = new PrintWriter(saveFile)) {
            printWriter.print(ip + ", " + port);
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ServerData getServerIp() {
        File saveFile = new File("./serverConfig");
        if (saveFile.exists()) {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(saveFile))) {
                String output = bufferedReader.readLine();
                bufferedReader.close();
                if (output != null && output.equals("")) {
                    return new ServerData("localhost", "8080");
                } else {
                    String[] outputSplit = output.split(", ");
                    return new ServerData(outputSplit[0], outputSplit[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return new ServerData("localhost", "8080");
            }
        } else return new ServerData("localhost", "8080");

    }

    public static List<Invoice> getGroupInvoices(SortingGroup sortingGroup, List<Invoice> invoices) {
        List<Invoice> groupInvoices = new ArrayList<>();
        boolean hasRequired = false;
        for (SortingData sortingData : sortingGroup.getSortingDataList()) {
            if (sortingData.isRequired()) {
                hasRequired = true;
                break;
            }
        }

        for (Invoice invoice : invoices) {
            boolean shouldAdd = false;
            if (hasRequired) {
                shouldAdd = true;
            }
            for (SortingData sortingData : sortingGroup.getSortingDataList()) {
                if (sortingData.getStringValue() != null) {
                    SortingResults sortingResults = processStringData(invoice, sortingData, hasRequired);
                    if (sortingResults.equals(SortingResults.ADD_BREAK)) {
                        shouldAdd = true;
                        break;
                    } else if (sortingResults.equals(SortingResults.DONT_ADD_BREAK)) {
                        shouldAdd = false;
                        break;
                    }
                } else if (sortingData.getDateValue() != null) {
                    SortingResults sortingResults = processDateValue(invoice, sortingData, hasRequired);
                    if (sortingResults.equals(SortingResults.ADD_BREAK)) {
                        shouldAdd = true;
                        break;
                    } else if (sortingResults.equals(SortingResults.DONT_ADD_BREAK)) {
                        shouldAdd = false;
                        break;
                    }
                } else if (sortingData.getNumberValue() != null) {
                    SortingResults sortingResults = processNumberData(invoice, sortingData, hasRequired);
                    if (sortingResults.equals(SortingResults.ADD_BREAK)) {
                        shouldAdd = true;
                        break;
                    } else if (sortingResults.equals(SortingResults.DONT_ADD_BREAK)) {
                        shouldAdd = false;
                        break;
                    }
                }
            }
            if (shouldAdd) {
                groupInvoices.add(invoice);
            }
        }
        return groupInvoices;
    }

    private static SortingResults processNumberData(Invoice invoice, SortingData sortingData, boolean hasRequired) {
        if (sortingData.getInvoiceProperties() != null) {
            if (sortingData.getInvoiceProperties().equals(InvoiceProperties.AMOUNT_LEFT)) {
                boolean areEqual = sortingData.getNumberValue().compareTo(invoice.getAmountLeft()) == 0;
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;
                }
            }
        } else if (sortingData.getCollNumber() != null) {
            boolean areEqual;
            if (invoice.getAdditionalValues().size() == 0) {
                areEqual = false;
            } else if (invoice.getAdditionalValues().get(sortingData.getCollNumber()).getDoubleValue() == null) {
                areEqual = false;
            } else {
                areEqual = sortingData.getNumberValue().compareTo(invoice.getAdditionalValues().get(sortingData.getCollNumber()).getDoubleValue()) == 0;
            }
            if (!hasRequired && areEqual) {
                return SortingResults.ADD_BREAK;
            } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                return SortingResults.DONT_ADD_BREAK;
            }

        }
        return SortingResults.CONTINUE;
    }

    private static SortingResults processDateValue(Invoice invoice, SortingData sortingData, boolean hasRequired) {
        if (sortingData.getInvoiceProperties() != null) {
            if (sortingData.getInvoiceProperties().equals(InvoiceProperties.ISSUE_DATE)) {
                boolean areEqual = invoice.getIssueDate().equals(sortingData.getDateValue());
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;
                }
            } else if (sortingData.getInvoiceProperties().equals(InvoiceProperties.PAY_DATE)) {
                boolean areEqual = invoice.getPayDate().equals(sortingData.getDateValue());
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;

                }
            }
        } else if (sortingData.getCollNumber() != null) {
            boolean areEqual;
            if (invoice.getAdditionalValues().size() == 0) {
                areEqual = false;
            } else if (invoice.getAdditionalValues().get(sortingData.getCollNumber()).getDateValue() == null) {
                areEqual = false;
            } else {
                areEqual = invoice.getAdditionalValues().get(sortingData.getCollNumber()).getDateValue().equals(sortingData.getDateValue());
            }
            if (!hasRequired && areEqual) {
                return SortingResults.ADD_BREAK;
            } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                return SortingResults.DONT_ADD_BREAK;
            }

        }
        return SortingResults.CONTINUE;
    }

    private static SortingResults processStringData(Invoice invoice, SortingData sortingData, boolean hasRequired) {
        if (sortingData.getInvoiceProperties() != null) {
            if (sortingData.getInvoiceProperties().equals(InvoiceProperties.CLIENT_NAME)) {
                boolean areEqual = SorterUtils.areEqual(invoice.getClientName(), sortingData.getStringValue());
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;
                }
            } else if (sortingData.getInvoiceProperties().equals(InvoiceProperties.ISSUER)) {
                boolean areEqual;
                if (invoice.getIssuer() == null) {
                    areEqual = false;
                } else {
                    areEqual = SorterUtils.areEqual(invoice.getIssuer(), sortingData.getStringValue());
                }
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;

                }
            } else if (sortingData.getInvoiceProperties().equals(InvoiceProperties.NUMBER)) {
                boolean areEqual = SorterUtils.areEqual(invoice.getNumber(), sortingData.getStringValue());
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;
                }
            } else if (sortingData.getInvoiceProperties().equals(InvoiceProperties.COMMENT)) {
                boolean areEqual = SorterUtils.areEqual(invoice.getComment(), sortingData.getStringValue());
                if (!hasRequired && areEqual) {
                    return SortingResults.ADD_BREAK;

                } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                    return SortingResults.DONT_ADD_BREAK;
                }
            }
        } else if (sortingData.getCollNumber() != null) {
            boolean areEqual;
            if (invoice.getAdditionalValues().size() == 0) {
                areEqual = false;
            } else if (invoice.getAdditionalValues().get(sortingData.getCollNumber()).getStringValue() == null) {
                areEqual = false;
            } else {
                areEqual = SorterUtils.areEqual(invoice.getAdditionalValues().get(sortingData.getCollNumber()).getStringValue(), sortingData.getStringValue());
            }
            if (!hasRequired && areEqual) {
                return SortingResults.ADD_BREAK;
            } else if (hasRequired && !areEqual && sortingData.isRequired()) {
                return SortingResults.DONT_ADD_BREAK;
            }
        }
        return SortingResults.CONTINUE;
    }

    public static boolean areEqual(String s1, String s2) {
        if (s1 == null || s2 == null) {
            return false;
        }
        final String s1m = s1.replaceAll("\\s+", "").toLowerCase();
        final String s2m = s2.replaceAll("\\s+", "").toLowerCase();

        return s1m.equals(s2m);
    }

    private enum SortingResults {
        ADD_BREAK, DONT_ADD_BREAK, CONTINUE
    }
}

package com.vesodev.sorterlauncher;

import com.vesodev.sorterlauncher.model.SoftwareVersion;
import com.vesodev.sorterlauncher.services.LauncherUtils;
import com.vesodev.sorterlauncher.services.ServerConnector;
import com.vesodev.sorterlauncher.services.VersionService;

import javax.swing.*;
import java.io.*;

/**
 * Created by vesko on 18.3.2020 г..
 */
public class SorterLauncher {

    public SoftwareVersion softwareVersion;

    private JProgressBar progressBar;
    private JWindow splash;

    public SorterLauncher() {
        initLookAndFeel();
        initSplash();

        softwareVersion = VersionService.getVersion();
        try {
            ServerConnector serverConnector = new ServerConnector(LauncherUtils.getServerUrl());
            SoftwareVersion latestVersion = serverConnector.getLatest(softwareVersion);
            if (latestVersion != null) {
                byte[] bytes = serverConnector.downloadVersionFile(latestVersion);
                VersionService.saveFile(bytes, latestVersion, softwareVersion);
                softwareVersion = latestVersion;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Process process = Runtime.getRuntime().exec("java -jar " + "jars/" + softwareVersion.getFileName());
            splash.setVisible(false);
            splash.dispose();
            BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            while (process.isAlive()) {
                String tmp;
                if ((tmp = outputReader.readLine()) != null) {
                    System.out.println(tmp);
                } else if ((tmp = errorReader.readLine()) != null) {
                    System.out.println(tmp);
                }
            }

            System.out.println(process.exitValue());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new SorterLauncher();
    }

    private void initSplash() {
        splash = new JWindow();
        splash.setSize(250, 60);
        splash.setLocationRelativeTo(null);
        splash.setLayout(null);

        progressBar = new JProgressBar();
        progressBar.setSize(220, 30);
        progressBar.setLocation(15, 15);
        progressBar.setIndeterminate(true);

        splash.add(progressBar);
        splash.setVisible(true);
    }

    private void initLookAndFeel() {
        try {
            UIManager.setLookAndFeel("com.jgoodies.looks.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    try {
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        return;
                    }
                }
            }
        }
    }

}

package com.vesodev.sorterlauncher.model;

/**
 * Created by vesko on 18.3.2020 г..
 */
public class SoftwareVersion {

    private Application application;

    private String version;

    private Integer year;

    private Integer month;

    private Integer patch;

    private String fileName;


    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getVersion() {
        version = year + "." + (month < 10 ? "0" + month : month) + (patch != null ? "." + patch : "");
        return version;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getPatch() {
        return patch;
    }

    public void setPatch(Integer patch) {
        this.patch = patch;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}

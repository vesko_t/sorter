package com.vesodev.sorterlauncher.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by vesko on 24.3.2020 г..
 */
public class LauncherUtils {

    public static String getServerUrl() {
        try (FileReader fileReader = new FileReader(new File("config"))) {
            try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
                String test = bufferedReader.readLine();
                System.out.println(test);
                return test;

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

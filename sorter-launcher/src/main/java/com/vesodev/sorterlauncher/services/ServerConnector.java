package com.vesodev.sorterlauncher.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.*;
import com.vesodev.sorterlauncher.model.SoftwareVersion;

import java.io.IOException;

/**
 * Created by vesko on 18.3.2020 г..
 */
public class ServerConnector {

    private static OkHttpClient httpClient;

    private boolean isConnected;
    private String serverUrl;

    public ServerConnector(String serverUrl) {
        this.serverUrl = serverUrl;
        httpClient = new OkHttpClient();
    }

    public SoftwareVersion getLatest(SoftwareVersion softwareVersion) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        Request request = new Request.Builder()
                .url(serverUrl + "/is-latest")
                .post(RequestBody.create(MediaType.parse("application/json"), objectMapper.writeValueAsString(softwareVersion)))
                .build();

        Response response = httpClient.newCall(request).execute();

        if (!response.isSuccessful()) {
            return null;
        }
        String body = response.body().string();


        if (body.equals("")) {
            return null;
        }

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(body, SoftwareVersion.class);
    }

    public byte[] downloadVersionFile(SoftwareVersion softwareVersion) throws IOException {
        Request request = new Request.Builder()
                .url(serverUrl + "/download-file?fileName=" + softwareVersion.getFileName())
                .build();
        Response response = httpClient.newCall(request).execute();

        if (!response.isSuccessful()) {
            return null;
        }

        return response.body().bytes();
    }

}

package com.vesodev.sorterlauncher.services;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vesodev.sorterlauncher.model.SoftwareVersion;

import java.io.*;

/**
 * Created by vesko on 21.3.2020 г..
 */
public class VersionService {

    private static final String applicationData = "./applicationData";

    public static void saveFile(byte[] bytes, SoftwareVersion softwareVersion, SoftwareVersion oldVersion) throws IOException {

        File dir = new File("jars");
        if (!dir.exists()) {
            dir.mkdir();
        }

        File file = new File("jars/" + softwareVersion.getFileName());
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bytes);
        saveVersionFile(softwareVersion);
        File oldVersionFile = new File("jars/" + oldVersion.getFileName());
        if (oldVersionFile.exists()) {
            oldVersionFile.delete();
        }
    }

    private static void saveVersionFile(SoftwareVersion softwareVersion) {
        File file = new File(applicationData);
        ObjectMapper objectMapper = new ObjectMapper();

        try (FileWriter fileWriter = new FileWriter(file)) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
                bufferedWriter.write(objectMapper.writeValueAsString(softwareVersion));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static SoftwareVersion getVersion() {
        File file = new File(applicationData);
        if (!file.exists()) {
            return null;
        }

        try (FileReader fileReader = new FileReader(file)) {
            try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
                StringBuilder stringBuilder = new StringBuilder();
                String tmp;
                while ((tmp = bufferedReader.readLine()) != null) {
                    System.out.println(tmp);
                    stringBuilder.append(tmp);
                    stringBuilder.append("\n");
                }

                ObjectMapper objectMapper = new ObjectMapper();

                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
                return objectMapper.readValue(stringBuilder.toString(), SoftwareVersion.class);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

package com.vesodev.sorterserver;

import com.vesodev.sorterserver.gui.Frame;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

@SpringBootApplication
public class SorterServerApplication {

    public static void main(String[] args) {
        try {
            setupSystemTray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SpringApplication.run(SorterServerApplication.class, args);
    }

    private static void setupSystemTray() throws IOException {
        if (!SystemTray.isSupported()) {
            JOptionPane.showMessageDialog(null,"SystemTray is not supported!");
            return;
        }
        final PopupMenu popup = new PopupMenu();
        final TrayIcon trayIcon = new TrayIcon(ImageIO.read(Frame.class.getResource("/images/icon64.png")));
        trayIcon.setImageAutoSize(true);
        final SystemTray tray = SystemTray.getSystemTray();

        MenuItem exitItem = new MenuItem("Exit");
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
        }
    }

}

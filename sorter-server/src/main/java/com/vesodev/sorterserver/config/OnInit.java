package com.vesodev.sorterserver.config;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;

@Component
public class OnInit {

    @PostConstruct
    public void afterBoot() {
        Thread thread = new Thread(() -> {
            try {
                JOptionPane.showMessageDialog(null, "Server started!", "Success", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception ignored) {

            }
        });
        thread.start();
    }

}

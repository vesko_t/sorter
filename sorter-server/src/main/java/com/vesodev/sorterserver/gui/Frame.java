package com.vesodev.sorterserver.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by vesko on 22.3.2020 г..
 */
public class Frame extends JFrame {

    private JTextArea consoleArea;

    public Frame(String title) {
        super(title);
        consoleArea = new JTextArea();
        setSize(new Dimension(500,600));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        GridLayout layout = new GridLayout();
        setLayout(layout);
        JScrollPane scrollPane = new JScrollPane(consoleArea);
        add(scrollPane);
        setVisible(true);
        setupConsole();

    }

    private void setupConsole(){
        consoleArea.setEditable(false);
        consoleArea.setLineWrap(true);
        TextAreaPrintStream printStream = new TextAreaPrintStream(consoleArea, System.out);
        System.setErr(printStream);
        System.setOut(printStream);

    }
}

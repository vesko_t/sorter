package com.vesodev.sorterserver.jpa;

import com.vesodev.sorterserver.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
}

package com.vesodev.sorterserver.jpa;

import com.vesodev.sorterserver.model.SortingData;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Aossia on 2/20/2020.
 */
public interface SortingDataRepository extends CrudRepository<SortingData, Integer> {
}

package com.vesodev.sorterserver.jpa;

import com.vesodev.sorterserver.model.SortingGroup;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Aossia on 2/20/2020.
 */
public interface SortingGroupRepository extends CrudRepository<SortingGroup, Integer> {
}

package com.vesodev.sorterserver.model;

/**
 * Created by Aossia on 2/20/2020.
 */
public enum  InvoiceProperties {
    ID, COMMENT, ISSUE_DATE, PAY_DATE, CLIENT_NAME, ISSUER, AMOUNT_LEFT, NUMBER
}

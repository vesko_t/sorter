package com.vesodev.sorterserver.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Date;

/**
 * Created by Aossia on 2/20/2020.
 */
@Entity
@Table(name = "sorting_data")
public class SortingData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Date dateValue;
    private Double numberValue;
    private String stringValue;
    private InvoiceProperties invoiceProperties;
    private Integer collNumber;
    private Boolean required;
    @ManyToOne()
    @JsonBackReference
    private SortingGroup sortingGroup;

    @Column(nullable = true)
    private Boolean isDate;

    @Column(nullable = true)
    private Boolean isNumber;

    @Column(nullable = true)
    private Boolean isString;

    public SortingData() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public Double getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(Double numberValue) {
        this.numberValue = numberValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public InvoiceProperties getInvoiceProperties() {
        return invoiceProperties;
    }

    public void setInvoiceProperties(InvoiceProperties invoiceProperties) {
        this.invoiceProperties = invoiceProperties;
    }

    public Integer getCollNumber() {
        return collNumber;
    }

    public void setCollNumber(Integer collNumber) {
        this.collNumber = collNumber;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public void setSortingGroup(SortingGroup sortingGroup) {
        this.sortingGroup = sortingGroup;
    }

    public SortingGroup getSortingGroup() {
        return sortingGroup;
    }

    public Boolean getDate() {
        return isDate;
    }

    public void setDate(Boolean date) {
        isDate = date;
    }

    public Boolean getNumber() {
        return isNumber;
    }

    public void setNumber(Boolean number) {
        isNumber = number;
    }

    public Boolean getString() {
        return isString;
    }

    public void setString(Boolean string) {
        isString = string;
    }
}

package com.vesodev.sorterserver.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

import java.util.List;

/**
 * Created by Aossia on 2/20/2020.
 */
@Entity
@Table(name = "sorting_groups")
public class SortingGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String groupName;

    @OneToMany(targetEntity = SortingData.class, mappedBy = "sortingGroup", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<SortingData> sortingDataList;

    public SortingGroup() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<SortingData> getSortingDataList() {
        return sortingDataList;
    }

    public void setSortingDataList(List<SortingData> sortingDataList) {
        this.sortingDataList = sortingDataList;
    }
}

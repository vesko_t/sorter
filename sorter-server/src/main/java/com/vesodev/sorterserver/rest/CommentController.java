package com.vesodev.sorterserver.rest;

import com.vesodev.sorterserver.jpa.CommentRepository;
import com.vesodev.sorterserver.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/comments")
public class CommentController {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @GetMapping("")
    public List<Comment> getAll() {
        return this.commentRepository.findAll();
    }

    @PostMapping("")
    public Comment saveComment(@RequestBody Comment comment) {
        return this.commentRepository.save(comment);
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable Integer id) {
        this.commentRepository.deleteById(id);
    }
}

package com.vesodev.sorterserver.rest;

import com.vesodev.sorterserver.jpa.SortingDataRepository;
import com.vesodev.sorterserver.jpa.SortingGroupRepository;
import com.vesodev.sorterserver.model.SortingData;
import com.vesodev.sorterserver.model.SortingGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Aossia on 2/20/2020.
 */
@RestController
@RequestMapping(path = "/api")
public class SortingDataController {

    @Autowired
    private SortingDataRepository sortingDataRepository;

    @Autowired
    private SortingGroupRepository sortingGroupRepository;

    @RequestMapping(path = "/save-group", method = RequestMethod.POST)
    public void saveGroup(@RequestBody SortingGroup sortingGroup) {
        SortingGroup group = sortingGroupRepository.save(sortingGroup);
        /*for (SortingData data : sortingGroup.getSortingDataList()) {
            data.setSortingGroup(group);
            sortingDataRepository.save(data);
        }*/
    }

    @RequestMapping(path = "delete-group", method = RequestMethod.POST)
    public void deleteGroup(@RequestBody SortingGroup sortingGroup) {
        for (SortingData data : sortingGroup.getSortingDataList()) {
            sortingDataRepository.delete(data);
        }
        sortingGroupRepository.delete(sortingGroup);
    }

    @RequestMapping(path = "delete-data", method = RequestMethod.POST)
    public void deleteSortingData(@RequestBody SortingData sortingData) {
        sortingDataRepository.delete(sortingData);
    }

    @RequestMapping(path = "get-groups", method = RequestMethod.GET)
    public List<SortingGroup> getGroups() {
        return (List<SortingGroup>) sortingGroupRepository.findAll();
    }
}

package com.vesodev.sorterupdateserver.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by vesko on 18.3.2020 г..
 */
@Entity
public class SoftwareVersion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Application application;

    private String version;

    private Integer year;

    private Integer month;

    private Integer patch;

    private String fileName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getVersion() {
        version = year + "." + (month < 10 ? "0" + month : month) + (patch != null ? "." + patch : "");
        return version;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getPatch() {
        return patch;
    }

    public void setPatch(Integer patch) {
        this.patch = patch;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @param softwareVersion the version to check
     * @return if softwareVersion is later than this return false else return true
     */
    public boolean isLater(SoftwareVersion softwareVersion) {
        if (year.compareTo(softwareVersion.getYear()) > 0) {
            return true;
        } else if (year.compareTo(softwareVersion.getYear()) < 0) {
            return false;
        } else {
            if (month.compareTo(softwareVersion.getMonth()) > 0) {
                return true;
            } else if (month.compareTo(softwareVersion.getMonth()) < 0) {
                return false;
            } else {
                if (patch == null && softwareVersion.getPatch() != null) {
                    return false;
                } else if (patch != null && softwareVersion.getPatch() == null) {
                    return true;
                } else if (patch == null && softwareVersion.getPatch() == null) {
                    return false;
                } else {
                    if (patch.compareTo(softwareVersion.getPatch()) > 0) {
                        return true;
                    } else if (patch.compareTo(softwareVersion.getPatch()) < 0) {
                        return false;
                    } else return false;
                }
            }
        }
    }


}

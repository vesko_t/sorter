package com.vesodev.sorterupdateserver.repositories;

import com.vesodev.sorterupdateserver.model.Application;
import com.vesodev.sorterupdateserver.model.SoftwareVersion;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by vesko on 18.3.2020 г..
 */
public interface VersionRepository extends CrudRepository<SoftwareVersion, Long> {

    public List<SoftwareVersion> findAllByApplication(Application application);
}

package com.vesodev.sorterupdateserver.rest;

import com.vesodev.sorterupdateserver.model.Application;
import com.vesodev.sorterupdateserver.model.SoftwareVersion;
import com.vesodev.sorterupdateserver.services.StorageService;
import com.vesodev.sorterupdateserver.services.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by vesko on 18.3.2020 г..
 */
@RestController
public class UpdateController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private VersionService versionService;

    @RequestMapping(path = "add-version", method = RequestMethod.POST)
    public void addVersion(@RequestBody SoftwareVersion softwareVersion) {
        versionService.save(softwareVersion);

    }

    @RequestMapping(path = "get-versions", method = RequestMethod.GET)
    public List<SoftwareVersion> getVersions(@RequestParam String app) {
        return versionService.findAllByApplication(Application.valueOf(app));
    }

    @RequestMapping(path = "is-latest", method = RequestMethod.POST)
    public SoftwareVersion isLatest(@RequestBody SoftwareVersion softwareVersion) {
        return versionService.getLatest(softwareVersion);
    }

    @RequestMapping(value = "/upload-file", method = RequestMethod.POST)
    public void uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        storageService.saveFile(file);
    }

    @RequestMapping(value = "/download-file", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(@RequestParam String fileName, HttpServletRequest request) throws IOException {
        Resource resource = storageService.getFile(fileName);

        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}

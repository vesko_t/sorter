package com.vesodev.sorterupdateserver.services;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by vesko on 18.3.2020 г..
 */
@Service
public class StorageService {

    private String path = "./files/";

    public void saveFile(MultipartFile multipartFile) throws IOException {
        if (multipartFile.isEmpty()) {

            throw new IOException("Failed to store empty file");
        }

        try {
            File file = new File(path);
            if (!file.exists()) {
                file.mkdir();
            }
            String fileName = multipartFile.getOriginalFilename();
            InputStream is = multipartFile.getInputStream();

            Files.copy(is, Paths.get(path + fileName),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    public Resource getFile(String fileName) throws IOException {
        try {
            Path fileStorageLocation = Paths.get(path)
                    .toAbsolutePath().normalize();
            Path filePath = fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new IOException("File not found " + fileName);
            }
        } catch (MalformedURLException e) {
            throw new IOException("File not found " + fileName);
        }
    }

}

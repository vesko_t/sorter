package com.vesodev.sorterupdateserver.services;

import com.vesodev.sorterupdateserver.model.Application;
import com.vesodev.sorterupdateserver.model.SoftwareVersion;
import com.vesodev.sorterupdateserver.repositories.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by vesko on 18.3.2020 г..
 */
@Service
public class VersionService {

    @Autowired
    private VersionRepository versionRepository;

    public SoftwareVersion getLatest(SoftwareVersion softwareVersion) {
        SoftwareVersion latest = null;
        for (SoftwareVersion selectedVersion : versionRepository.findAllByApplication(softwareVersion.getApplication())) {
            if(selectedVersion.isLater(softwareVersion)){
                latest = selectedVersion;
            }
        }
        return latest;
    }

    public List<SoftwareVersion> findAllByApplication(Application application) {
        return versionRepository.findAllByApplication(application);
    }

    public void save(SoftwareVersion softwareVersion) {
        versionRepository.save(softwareVersion);
    }
}
